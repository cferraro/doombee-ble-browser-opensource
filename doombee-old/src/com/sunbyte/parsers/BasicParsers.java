package com.sunbyte.parsers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BasicParsers {
	
	public static boolean tryParseInt(String value)  
	{  
	      try {
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe) {  
	          return false;  
	      }  
	}
	
	
	public static boolean tryParseDate(String value)
	{
		try {
			new SimpleDateFormat().parse(value);
			return true;
		} catch (ParseException e) {
			return false;
		}
		
	}
	
	
	public static String ParseTimeFromInt(int time) {
		
		 String timeStr = Integer.toString(time);
		 if (timeStr.length() == 3) {
			 timeStr = "0" + timeStr;
		 }
		 
		 DateFormat parser = new SimpleDateFormat("HHmm");
		 Date resultTime;
		 String resultString = "";
		 
		 try {
			resultTime = parser.parse(timeStr);
			String minutes, hours;
			hours = Integer.toString(resultTime.getHours());
			if (resultTime.getMinutes() == 0) {
				minutes = Integer.toString(resultTime.getMinutes()) + "0";
			} else {
				minutes = Integer.toString(resultTime.getMinutes());
			}
			
			resultString = hours + ":" + minutes;    			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return resultString;
	}
	
	
	public static Date ParseTimeFromIntToDate(int time) {
		
		 String timeStr = Integer.toString(time);
		 if (timeStr.length() == 3) {
			 timeStr = "0" + timeStr;
		 }
		 
		 DateFormat parser = new SimpleDateFormat("HHmm");
		 Date resultTime = null;

		 try {
			resultTime = parser.parse(timeStr);  			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return resultTime;
	}
	
	
}
