package com.sunbyte.dombee;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import kn.uni.voronoitreemap.datastructure.OpenList;
import kn.uni.voronoitreemap.diagram.PowerDiagram;
import kn.uni.voronoitreemap.j2d.PolygonSimple;
import kn.uni.voronoitreemap.j2d.Site;

import com.sunbyte.classes.BLEDevice;
import com.sunbyte.classes.UUIDInfo;
import com.sunbyte.doombee.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {

	BrowserView bView;
	
	private BluetoothAdapter mBluetoothAdapter;
	private boolean mScanning;


	
	private static final boolean AUTO_HIDE = true;
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
	private static final boolean TOGGLE_ON_CLICK = true;
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
	private SystemUiHider mSystemUiHider;
	private final static int REQUEST_ENABLE_BT = 1;
	
	
	// list of devices
	//ArrayList<BluetoothDevice> blofDevices;
	//ArrayList<BluetoothDevice> blofDevicesTemp;
	ArrayList<BLEDevice> blofDevices;
	
	
	private int mInterval = 20000; // 5 seconds by default, can be changed later
	// Stops scanning after 5 seconds.
	private static final long SCAN_PERIOD = 5000;
	
	private Handler mHandler;
	private Handler mHandlerMain;
	
	private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }

    }
	
	//private LeDeviceListAdapter mLeDeviceListAdapter;
	// Device scan callback.
	private BluetoothAdapter.LeScanCallback mLeScanCallback =
	        new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
	        runOnUiThread(new Runnable() {
		           @Override
		           public void run() {
		        	  
		        	  
		        	  boolean exists = false;
		        	  for (int i = 0; i < blofDevices.size(); i++) {
		        		  
		        		  BluetoothDevice compareDevice = blofDevices.get(i).getDevice();
		        		  if (device.getAddress().compareTo(compareDevice.getAddress()) == 0) {
		        			  blofDevices.get(i).setLastUpdated(new Date());
		        			  
		        			  double newdistance = getDistanceFromScan(scanRecord, rssi);
		        			  blofDevices.get(i).getUUIDInfo().setDistance(newdistance);
		        			  
		        			  exists = true;
		        			  break;
		        		  }
		        	  }
		        	  if (!exists) {
		        		  
		        		  BLEDevice newDevice = new BLEDevice();
		        		  newDevice.setDevice(device);
		        		  newDevice.setLastUpdated(new Date());
		        		  
		        		  UUIDInfo uuidinfo = getUUIDInfoFromScan(scanRecord, rssi);
		        		  newDevice.setUUIDInfo(uuidinfo);
		        		  
		        		  System.out.println("ADDED DEVICE" + device.getAddress() + " rssi " +  Integer.toString(rssi) + " scanRecord " + scanRecord);
		        		  blofDevices.add(newDevice);
		        		  
		        		  // finally refresh interface
		        		  bView.invalidate();
		        		  bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);
		        	 
		        	  } else {
		        		  
		        		  // finally refresh interface
		        		  // todo this invalidate is called too 
		        		  bView.invalidate();
		        		  bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);
		        	  }
		        	  
	        	      // clean up 
	        	      // remove out of range devices: not updated since
	        		  for (int i = blofDevices.size() - 1; i >= 0; i--) {
	        			  Date checkdate = blofDevices.get(i).getLastUpdated();
	        			  Date currentDate = new Date();
	        			  
	        			  long seconds = (currentDate.getTime()-checkdate.getTime())/1000;
	        			  
	        			  long timeoutLimit = (SCAN_PERIOD + (mInterval / 2)) / 1000;
	        			 // System.out.println("cur" + currentDate.getTime() + " CHECK" + checkdate.getTime() + " SECONDS " + seconds + " timout " + timeoutLimit);
	        			  if (seconds > timeoutLimit) {
	        				  // remove device, it's out of range
	        				  blofDevices.remove(i);
		        		  	  bView.invalidate();
		        		  	  bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);

	        			  }
	        		  }
		        	  
		        	   //mLeDeviceListAdapter.addDevice(device);
		               //mLeDeviceListAdapter.notifyDataSetChanged();
		           }
		       });
			
		}
	};
	
	
	
	
	private static double getDistanceFromScan(byte[] scanRecord, int rssi) {
		int startByte = 2;
	  	  boolean patternFound = false;
	  	  while (startByte <= 5) {
	  	      if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
	  	              ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
	  	            patternFound = true;
	  	            break;
	  	      }
	  	      startByte++;
	  	  }
	  	  
	  	  double distance = 0.0;
	  	  if (patternFound) {
	  		  int txpower = (scanRecord[startByte+24] & 0xff);
	  	      distance = calculateDistance(txpower, rssi);  
	  	  }
		return distance;
	}
	
	/** 
	 * getUUIDAndMajorMinorFromScan
	 */
	private static UUIDInfo getUUIDInfoFromScan(byte[] scanRecord, int rssi) {
	  	  
		int startByte = 2;
	  	  boolean patternFound = false;
	  	  while (startByte <= 5) {
	  	      if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
	  	              ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
	  	            patternFound = true;
	  	            break;
	  	      }
	  	      startByte++;
	  	  }
	  	    
	  	  if (patternFound) {
	  	      //Convert to hex String
	  	      byte[] uuidBytes = new byte[16];
	  	      System.arraycopy(scanRecord, startByte+4, uuidBytes, 0, 16);
	  	      String hexString = bytesToHex(uuidBytes);
	  	        
	  	      //Here is your UUID
	  	      String uuid =  hexString.substring(0,8) + "-" + 
	  	    		  		 hexString.substring(8,12) + "-" + 
	  	    		  		 hexString.substring(12,16) + "-" + 
	  	    		  		 hexString.substring(16,20) + "-" + 
	  	    		  		 hexString.substring(20,32);
	  	        
	  	      //Here is your Major value
	  	      int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);
	  	      //Here is your Minor value
	  	      int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);
	  	     // System.out.println("UUID" + uuid + "Major " + major + " Minor " + minor);
	  	     
	  	      int txpower = (scanRecord[startByte+24]);
	  	      
	  	      double distance = calculateDistance(txpower, rssi);
	  	    //  System.out.println("TXPOWER " + txpower + "  " + scanRecord[startByte+24] + " DISTANCE " + distance);
	  	      UUIDInfo uuidinfo = new UUIDInfo(uuid, major, minor, txpower, distance);
	  	      return uuidinfo;
	  	      
	  	  } else {
	  		  
	  		  return null;
	  	  }
	  	  
	}
	
	
	protected static double calculateDistance(int txPowerMeter, double rssi) {
		  if (rssi == 0) {
		    return -1.0; // if we cannot determine distance, return -1.
		  }

		  double ratio = rssi*1.0/txPowerMeter;
		  if (ratio < 1.0) {
		    return Math.pow(ratio,10);
		  }
		  else {
		    double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;    
		    return accuracy;
		  }
	} 
	
	/**
	 * bytesToHex method
	 * Found on the internet
	 * http://stackoverflow.com/a/9855338
	 */
	static final char[] hexArray = "0123456789ABCDEF".toCharArray();
	private static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	
	
	Runnable mScannerRunTask = new Runnable() {
	    @Override  
	    public void run() {
	    	scanLeDevice(true);
	    	mHandlerMain.postDelayed(mScannerRunTask, mInterval);
	    }
	  };

	  void startRepeatingTask() {
		  mScannerRunTask.run(); 
	  }

	  void stopRepeatingTask() {
		  mHandlerMain.removeCallbacks(mScannerRunTask);
	  }
	  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		
		// create Browser
		bView = new BrowserView(getBaseContext(), 0, new ArrayList<BLEDevice>(), this);
		
		setContentView(bView);

		
		
		// is BLE supported
		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			// BLE is not supported 
		    //finish();
		}
		
		
		// is Bluetooth enabled
		
		//final BluetoothManager bluetoothManager =
		//        (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			// bluetooth is disabled
			System.out.println("Bluetooth is disabled");
			//    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		//    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}

		
		blofDevices = new ArrayList<BLEDevice>();
	    mHandler = new Handler();
		mHandlerMain = new Handler();
		
		 // start ble scanner
		this.startRepeatingTask();
		
		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		

		//mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
//		mSystemUiHider.setup();
//		mSystemUiHider
//				.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
//					// Cached values.
//					int mControlsHeight;
//					int mShortAnimTime;
//
//					@Override
//					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
//					public void onVisibilityChange(boolean visible) {
//						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//							// If the ViewPropertyAnimator API is available
//							// (Honeycomb MR2 and later), use it to animate the
//							// in-layout UI controls at the bottom of the
//							// screen.
//							if (mControlsHeight == 0) {
//								mControlsHeight = controlsView.getHeight();
//							}
//							if (mShortAnimTime == 0) {
//								mShortAnimTime = getResources().getInteger(
//										android.R.integer.config_shortAnimTime);
//							}
//							controlsView
//									.animate()
//									.translationY(visible ? 0 : mControlsHeight)
//									.setDuration(mShortAnimTime);
//						} else {
//							// If the ViewPropertyAnimator APIs aren't
//							// available, simply show or hide the in-layout UI
//							// controls.
//							controlsView.setVisibility(visible ? View.VISIBLE
//									: View.GONE);
//						}
//
//						if (visible && AUTO_HIDE) {
//							// Schedule a hide().
//							delayedHide(AUTO_HIDE_DELAY_MILLIS);
//						}
//					}
//				});

		// Set up the user interaction to manually show or hide the system UI.
//		contentView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				if (TOGGLE_ON_CLICK) {
//					mSystemUiHider.toggle();
//				} else {
//					mSystemUiHider.show();
//				}
//			}
//		});

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
//		findViewById(R.id.dummy_button).setOnTouchListener(
//				mDelayHideTouchListener);
	}

//	@Override
//	protected void onPostCreate(Bundle savedInstanceState) {
//		super.onPostCreate(savedInstanceState);
//
//		// Trigger the initial hide() shortly after the activity has been
//		// created, to briefly hint to the user that UI controls
//		// are available.
//		delayedHide(100);
//	}

	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
//	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
//		@Override
//		public boolean onTouch(View view, MotionEvent motionEvent) {
//			if (AUTO_HIDE) {
//				delayedHide(AUTO_HIDE_DELAY_MILLIS);
//			}
//			return false;
//		}
//	};

//	Handler mHideHandler = new Handler();
//	Runnable mHideRunnable = new Runnable() {
//		@Override
//		public void run() {
//			mSystemUiHider.hide();
//		}
//	};

	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
//	private void delayedHide(int delayMillis) {
//		mHideHandler.removeCallbacks(mHideRunnable);
//		mHideHandler.postDelayed(mHideRunnable, delayMillis);
//	}
}
