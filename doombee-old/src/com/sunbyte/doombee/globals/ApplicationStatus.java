package com.sunbyte.doombee.globals;

import java.util.List;


import android.content.Context;
import android.content.SharedPreferences;

public class ApplicationStatus {

	
	
	//public static final String ServiceBase = "dns.doombee.com/webservice/BLEdns/";
	public static final String ServiceBase = "http://dns.doombee.com/webservice/BLEdns";
	//public static final String HtmlBase = "http://wg.vod.ftctele.com/rvil/aisteps/";
	public static String ProgramName = "Doombee 0.1";
   
	public static String MainPackage = "com.sunbyte.doombee";
	
	//public static InterfaceEnum curActivity;
	
	// SHARED PREFERENCES FOR CREDENTIALS

	private final static String LASTFRAGMENT_SHAREDPREFERENCES = "LastFragment";
	private static SharedPreferences AppSharedLastFragment;
	private static String LAST_FRAGMENT = "lastFragment";
	
	public static int GetLastFragmentStatus(Context ctx) {
		AppSharedLastFragment = ctx.getSharedPreferences(LASTFRAGMENT_SHAREDPREFERENCES, Context.MODE_PRIVATE);
		return AppSharedLastFragment.getInt(LAST_FRAGMENT, 0);
	}

	
	public static void SetLastFragmentStatus(int value) {
		if (AppSharedLastFragment != null) {
			SharedPreferences.Editor editor = AppSharedLastFragment.edit();
			editor.putInt(LAST_FRAGMENT, value);
			editor.commit();
		}
	}

}
