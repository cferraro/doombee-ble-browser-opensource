package com.sunbyte.doombee.ui;



import java.util.ArrayList;
import java.util.List;

import com.sunbyte.classes.BLECharacteristic;
import com.sunbyte.classes.BLEService;
import com.sunbyte.classes.CharacteristicCRUD;
import com.sunbyte.dombee.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class CustomDialog extends Dialog {

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public CustomDialog(Context context) {
        super(context);
    }

    /**
     * Helper class for creating a custom dialog
     */
    public static class Builder {

        private Context context;
        private String title;
        private String message;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;
        public View layout;
        public CustomDialog dialog;
        private List<BLEService> listb;
        
        CharacteristicCRUD BLECRUD;
        
        private DialogInterface.OnClickListener 
                        positiveButtonClickListener,
                        negativeButtonClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        
        public CustomDialog createTableLayout() {
        	
        	TableRow inflateRow = null;

        	//Main TableLayout

        	TableLayout tblAddLayout = (TableLayout) layout.findViewById(R.id.table_custom_settings);

        	
        	for (int i=0;i<listb.size();i++){

        	    inflateRow = (TableRow) View.inflate(context, R.layout.dialog_table_row, null);
        	    
        	    TextView text1 = (TextView) inflateRow.findViewById(R.id.serviceName);
        	    TextView text2 = (TextView) inflateRow.findViewById(R.id.serviceUUID);
        	    
        	    text1.setText(listb.get(i).getName());
        	    text2.setText(listb.get(i).getUUID());
        	    
        	    //set tag for each TableRow
        	    inflateRow.setTag(i);
        	    //add TableRows to TableLayout
        	    tblAddLayout.addView(inflateRow);
        	    //set click listener for all TableRows
        	    inflateRow.setOnClickListener(ServiceRowClick);
        	}
            dialog.setContentView(layout);
            return dialog;
            //return dialog;
        	//public void onClick(View v){

        	//    String strTag = v.getTag().toString();
        	// Find the corresponding TableRow from the Main TableLayout using the tag when you click.

        	//    TableRow tempTableRow = (TableRow)tblAddLayout.findViewWithTag(strTag); 
        	    //then add your layout in this TableRow tempTableRow.
        	//}
        	
        }
        

        /**
         * Set list of BLE services for Dialog
         * @param negativeButtonText
         * @param listener
         * @return
         */
        public void setServicesList(List<BLEService> listb) {
        	this.listb = listb;
        }
    
        public void setBLECRUDDevice(CharacteristicCRUD blecrud) {
        	this.BLECRUD = blecrud;
        }
        
        View.OnClickListener ServiceRowClick = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
	        	
				String strTag = v.getTag().toString();
	        	// Find the corresponding TableRow from the Main TableLayout using the tag when you click.
				
				TableRow inflateRow = null;
	        	TableLayout tblServicesLayout = (TableLayout) layout.findViewById(R.id.table_custom_settings);
	        	TableLayout tblCharacteristicsLayout = (TableLayout) layout.findViewById(R.id.table_custom_characteristics);
	        	tblServicesLayout.setVisibility(View.GONE);
	        	tblCharacteristicsLayout.setVisibility(View.VISIBLE);
	        	
	        	//TableRow tempTableRow = (TableRow)tblAddLayout.findViewWithTag(strTag); 
	        	final ArrayList<BLECharacteristic> listbc = listb.get(Integer.parseInt(strTag)).getListOfCharacteristics();
	        	
	        	// run this on threads
	        	Handler mBackgroundHandler;
	        	mBackgroundHandler.post(new Runnable() {
	        	    @Override
	        	    public void run() {
	        	        new Thread() {
	        	            @Override
	        	            public void run() {
	        	            	BLECRUD.readAllCharacteristicsFromList(listbc);
	        	            }
	        	        }.start();
	        	    }
	        	});
	        	
	        	
	        	
	        	
	        	
	        	// Wait until
	        	
	        	
	        	for (int i=0;i<listbc.size();i++){

	        	    inflateRow = (TableRow) View.inflate(context, R.layout.characteristics_table_row, null);
	        	    
	        	    TextView text1 = (TextView) inflateRow.findViewById(R.id.characteristicName);
	        	    TextView text2 = (TextView) inflateRow.findViewById(R.id.characteristicUUID);
	        	    TextView text3 = (TextView) inflateRow.findViewById(R.id.characteristicValue);
	        	    
	        	    text1.setText(listbc.get(i).getName());
	        	    text2.setText(listbc.get(i).getUUID());
	        	    text3.setText(text);
	        	    
	        	    
	        	    //set tag for each TableRow
	        	    inflateRow.setTag(i);
	        	    //add TableRows to TableLayout
	        	    tblCharacteristicsLayout.addView(inflateRow);
	        	    //set click listener for all TableRows
	        	    //inflateRow.setOnClickListener(ServiceRowClick);
	        	}
			}
        
        };
        
        /**
         * Set the Dialog message from String
         * @param title
         * @return
         */
        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        /**
         * Set the Dialog message from resource
         * @param title
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        /**
         * Set the Dialog title from resource
         * @param title
         * @return
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        /**
         * Set the Dialog title from String
         * @param title
         * @return
         */
        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }
        
        /**
         * Set a custom content view for the Dialog.
         * If a message is set, the contentView is not
         * added to the Dialog...
         * @param v
         * @return
         */
        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         * @param positiveButtonText
         * @param listener
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText,
                DialogInterface.OnClickListener listener) {
            this.positiveButtonText = (String) context
                    .getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        /**
         * Set the positive button text and it's listener
         * @param positiveButtonText
         * @param listener
         * @return
         */
        public Builder setPositiveButton(String positiveButtonText,
                DialogInterface.OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        /**
         * Set the negative button resource and it's listener
         * @param negativeButtonText
         * @param listener
         * @return
         */
        public Builder setNegativeButton(int negativeButtonText,
                DialogInterface.OnClickListener listener) {
            this.negativeButtonText = (String) context
                    .getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        /**
         * Set the negative button text and it's listener
         * @param negativeButtonText
         * @param listener
         * @return
         */
        public Builder setNegativeButton(String negativeButtonText,
                DialogInterface.OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }
    

        
        
        /**
         * Create the custom dialog
         */
        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            dialog = new CustomDialog(context, 
                        R.style.Dialog);
            
            layout = inflater.inflate(R.layout.dialog_custom_settings, null);
            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title
            ((TextView) layout.findViewById(R.id.title)).setText(title);
            // set the confirm button
            if (positiveButtonText != null) {
                ((Button) layout.findViewById(R.id.positiveButton))
                        .setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    ((Button) layout.findViewById(R.id.positiveButton))
                            .setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    positiveButtonClickListener.onClick(
                                                dialog, 
                                            DialogInterface.BUTTON_POSITIVE);
                                }
                            });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(R.id.positiveButton).setVisibility(
                        View.GONE);
            }
            // set the cancel button
            if (negativeButtonText != null) {
                ((Button) layout.findViewById(R.id.negativeButton))
                        .setText(negativeButtonText);
                if (negativeButtonClickListener != null) {
                    ((Button) layout.findViewById(R.id.negativeButton))
                            .setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    positiveButtonClickListener.onClick(
                                                dialog, 
                                            DialogInterface.BUTTON_NEGATIVE);
                                }
                            });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(R.id.negativeButton).setVisibility(
                        View.GONE);
            }
            // set the content message
            if (message != null) {
                //((TextView) layout.findViewById(
                //                R.id.message)).setText(message);
            } else if (contentView != null) {
                // if no message set
                // add the contentView to the dialog body
                ((LinearLayout) layout.findViewById(R.id.content))
                        .removeAllViews();
                ((LinearLayout) layout.findViewById(R.id.content))
                        .addView(contentView, 
                                new LayoutParams(
                                        LayoutParams.WRAP_CONTENT, 
                                        LayoutParams.WRAP_CONTENT));
            }
            dialog.setContentView(layout);
            return dialog;
        }

    }
    
}