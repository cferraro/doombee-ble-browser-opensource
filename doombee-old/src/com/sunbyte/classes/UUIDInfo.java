package com.sunbyte.classes;

public class UUIDInfo {
	
	public String UUID;
	public int major;
	public int minor;
	public int txpower;
	public double distance;
	
	
	public UUIDInfo(String uuid, int major, int minor, int txpower, double distance) {
		this.UUID = uuid;
		this.major = major;
		this.minor = minor;
		this.txpower = txpower;
	}
	
	public void setUUID(String uuid) {
		this.UUID = uuid;
	}
	public String getUUID() {
		return this.UUID;
	}
	
	public void setMajor(int major) {
		this.major = major;
	}
	
	public int getMajor() {
		return this.major;
	}
	
	public void setMinor(int minor) {
		this.minor = minor;
	}
	
	public int getMinor() {
		return this.minor;
	}
	
	public int getTxPower() {
		return this.txpower;
	}
	
	public void setTxPower(int txpower) {
		this.txpower = txpower;
	}
	
	public double getDistance() {
		return this.distance;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
}
