package com.sunbyte.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

import com.sunbyte.services.BluetoothLeService;

import android.bluetooth.BluetoothGattCharacteristic;

public class CharacteristicCRUD {
	
	public ArrayList<String> outValues;
	public ArrayList<String> inValues;
	private BluetoothLeService mBluetoothLeService;
	
	boolean isReading;
	ReadLock readLock;
	
	public CharacteristicCRUD(BluetoothLeService mBLS) {
		this.mBluetoothLeService = mBLS;
		this.AysncInputReady = false;
	}
	
	
	public void setAsyncInputIsReady(boolean value) {

	    synchronized (readLock) {
	        isReading = false;
	        readLock.notifyAll();
	    }
	}
	
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		
	}
	
	
	public void readAllCharacteristicsFromList(ArrayList<BLECharacteristic> list) {
		
		for (int i = 0; i < list.size(); i++) {
			
			BluetoothGattCharacteristic gattc = list.get(i).gattCharacteristic;
			this.mBluetoothLeService.readCharacteristic(gattc);
			
			while (isReading) {
		        synchronized (readLock) {
		            try {
		                readLock.wait();
		            } catch (InterruptedException e) {
		                e.printStackTrace();
		            }
		        }
		    }
			
		}
		
	}


	private void doRead() {
	    //....internal accounting stuff up here....
	    characteristic = mGatt.getService(mCurrServiceUUID).getCharacteristic(mCurrCharacteristicUUID);
	    isReading = mGatt.readCharacteristic(characteristic);

	    showToast("Is reading in progress? " + isReading);

	    showToast("On thread: " + Thread.currentThread().getName());

	    // Wait for read to complete before continuing.
	    while (isReading) {
	        synchronized (readLock) {
	            try {
	                readLock.wait();
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

	public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
	    showToast("onCharacteristicRead()");
	    showToast("On thread: " + Thread.currentThread().getName());

	    byte[] value = characteristic.getValue();
	    StringBuilder sb = new StringBuilder();
	    for (byte b : value) {
	        sb.append(String.format("%02X", b));
	    }

	    showToast("Read characteristic value: " + sb.toString());

	    synchronized (readLock) {
	        isReading = false;
	        readLock.notifyAll();
	    }
	}
}
