package com.sunbyte.classes;

import java.util.Date;

import android.bluetooth.BluetoothDevice;
import android.media.Image;

public class BLEDevice {
	
	public BluetoothDevice bluDevice;
	public int weight;
	public Integer color;
	public Image logo;
	
	public UUIDInfo uuidinfo;
	
	public Date lastUpdated;
	
	public void setUUIDInfo(UUIDInfo uuidinfo) {
		this.uuidinfo = uuidinfo;
	}
	
	public UUIDInfo getUUIDInfo() {
		return this.uuidinfo;
	}
	
	public void setDevice(BluetoothDevice bluDevice) {
		this.bluDevice = bluDevice;
	}
	
	public BluetoothDevice getDevice() {
		return this.bluDevice;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public int getWeight() {
		return this.weight;
	}
	
	public void setColor(int color) {
		this.color = color;
	}
	
	public Integer getColor() {
		return this.color;
	}
	
	public void setLogo(Image logo) {
		this.logo = logo;
	}
	
	public Image getLogo() {
		return logo;
	}
	
	public void setLastUpdated(Date lastupdated) {
		this.lastUpdated = lastupdated;
	}
	
	public Date getLastUpdated() {
		return this.lastUpdated;
	}
	
}
