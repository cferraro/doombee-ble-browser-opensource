package com.sunbyte.classes;

import java.util.ArrayList;
import java.util.List;

import android.bluetooth.BluetoothGattService;

public class BLEService {

	public String UUID;
	public String Name;
	public int defIcon;
	public ArrayList<BLECharacteristic> listOfCharacteristics;
	BluetoothGattService gattService;
	
	public BluetoothGattService getgattService() {
		return gattService;
	}
	
	public void setgattService(BluetoothGattService bGattService) {
		this.gattService = bGattService;
	}
	
	
	public String getUUID() {
		return UUID;
	}
	
	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
	
	public String getName() {
		return this.Name;
	}
	
	public void setName(String name) {
		this.Name = name;
	}
	
	public int getDefIcon() {
		return this.defIcon;
	}
	
	public void setDefIcon(int defIcon) {
		this.defIcon = defIcon;
	}
	
	public void setListOfCharacteristics(ArrayList<BLECharacteristic> listc) {
		this.listOfCharacteristics = listc;
	}
	
	public ArrayList<BLECharacteristic> getListOfCharacteristics() {
		return this.listOfCharacteristics;
	}
}
