package com.sunbyte.classes;

import java.util.Date;


//<id>1</id>
//<domain>mybledomainname</domain>
//<fullURI>http://www.returnedurl.ble</fullURI>
//<MACAddress>
//  <MACAddre>324.324.132.231</MACAddre>
//</MACAddress>
//<UUID>hello</UUID>
//<Major>1000</Major>
//<Minor>100</Minor>
//<iconURI>icon</iconURI>
//<color>color</color>
//<rank>10</rank>
//<gpslocation>22321312</gpslocation>
//<created>2015-08-10 00:00:00</created>
//<modified>2015-08-04 00:00:00</modified>


public class NetworkEntity {
	int id;
	String domain;
	String fullURI;
	String MACAddress;
	String UUID;
	int Major;
	int Minor;
	String iconURI;
	String color;
	int rank;
	String gpslocation;
	String created;
	String modified;
	
	public int getId() {
		return this.id;
	}
	
	public String getfullURI() {
		return fullURI;
	}
	
	public void setfullURI(String fullURI) {
		this.fullURI = fullURI;
	}
	
	public String getdomain() {
		return this.domain;
	}
	
	public String getMACAddress() {
		return this.MACAddress;
	}
	
	public String getUUID() {
		return this.UUID;
	}
	
	public int getMajor() {
		return this.Major;
	}
	
	public int getMinor() {
		return this.Minor;
	}
	
	public String geticonURI() {
		return this.iconURI;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public String getgpslocation() {
		return this.gpslocation;
	}
	
	public String getcreated() {
		return this.created;
	}
	
	public String getmodified() {
		return this.modified;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setdomain(String domain) {
		this.domain = domain;
	}
	
	public void setMACAddress(String MACAddress) {
		this.MACAddress = MACAddress;
	}
	
	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
	
	public void setMajor(int major) {
		this.Major = major;
	}
	
	public void setMinor(int minor) {
		this.Minor = minor;
	}
	
	public void seticonURI(String iconURI) {
		this.iconURI = iconURI;
	}
	
	public void getColor(String color) {
		this.color = color;
	}
	
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public void setgpslocation(String gpsloc) {
		this.gpslocation = gpsloc;
	}
	
	public void setcreated(String created) {
		this.created = created;
	}
	
	public void setmodified(String modified) {
		this.modified = modified;
	}
	
}
