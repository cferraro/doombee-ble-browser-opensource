﻿<?php

include '../main/index.php';

//error_reporting(-1);
//ini_set('display_errors', 'On');

class BLEdns {
	
    private $db;
	private $rest;
	
	const DB_prefix = "";
	const DB_TABLE = "Network";
	
	const EntityName = "network";
	
	private static $TABLE_ARRAY_STRUCT = array(
			"COLUMN_ID" => "id",
			"COLUMN_DOMAIN" => "domain",
			"COLUMN_FULLURI" => "fullURI",
			"COLUMN_MAC_ADDRESS" => "MACAddress",
			"COLUMN_UUID" => "UUID",
			"COLUMN_MAJOR" => "Major",
			"COLUMN_MINOR" => "Minor",
			"COLUMN_ICONURI" => "iconURI",
			"COLUMN_COLOR" => "color",
			"COLUMN_RANK" => "rank",
			"COLUMN_GPSLOCATION" => "gpslocation",
			"COLUMN_CREATED" => "created",
			"COLUMN_MODIFIED" => "modified"
			);
	private static $TABLE_ARRAY_TYPES = array('i', 's', 's', 's', 's', 's', 's', 's', 's', 'i', 's', 's', 's');
	
	
	
	public function setDb($db) {
		$this->db = $db;	
	}
 
	public function setRest($rest) {
		$this->rest = $rest;
	}
	
	
	public function ResolveDevice($uuid, $minor, $major, $mac, $url) {
	
		$curTable = self::DB_prefix . self::DB_TABLE;
		$theTable = self::DB_TABLE;
	
		$minorSql = '';
		$majorSql = '';
		$macSql = '';
		$urlSql = '';
		
		if (!isset($url)) {
			
			$uuidSql = "UUID = '" . $uuid . "'";
			if (isset($minorSql)) {
				$minorSql = " AND Minor = '" . $minor . "'";
			}
			if (isset($majorSql)) {
				$majorSql = " AND Major = '" . $major . "'";
			}
			if (isset($mac)) {
				$macSql = " AND MACAddress = '" . $mac . "'";
			}
		
		} else {
			$urlSql = "fullURI = '" . $url . "'";
		}
		$stmt = $this->db->prepare('SELECT id, domain, fullURI, MACAddress, iconURI, color, rank, gpslocation, created, modified FROM ' . $curTable . " WHERE " . $uuidSql . $minorSql . $majorSql . $macSql);
		//$stmt->bind_param("sii", $uid, $limitStart, $limit);
		
		//if (!isset($url)) {
		//	$stmt->bind_param('siiss', $uuid, $minor, $major, $mac, $url);
		//} else {
		//	$stmt->bind_param('siiss', $url);
		//}
		$stmt->execute();
		$stmt->bind_result($id, $domain, $fullURI, $MACAddress, $iconURI, $color, $rank, $gpslocation, $created, $modified);
	
		$cnt = 0;
		$result = null;
		$result["$theTable"][] = array();
		
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
	
		while ($stmt->fetch()) {
			$cnt++;
			$result["$theTable"][] = array(
			
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] => $id,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[1]"] => $domain,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[2]"] => $fullURI,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[3]"] => $MACAddress,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[4]"] => $uuid,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[5]"] => $major,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[6]"] => $minor,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[7]"] => $iconURI,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[8]"] => $color,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[9]"] => $rank,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[10]"] => $gpslocation,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[11]"] => $created,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[12]"] => $modified,
			);
		}
		$stmt->close();
	
		return $result;
	}
	
	
	public function getExercises() {

		$curTable = self::DB_prefix . self::DB_TABLE;
		$theTable = self::DB_TABLE;
		
		$stmt = $this->db->prepare('SELECT * FROM ' . $curTable);
		//$stmt->bind_param("sii", $uid, $limitStart, $limit);
		$stmt->execute();
		$stmt->bind_result($id, $trainingtype, $name, $lastmodified, $deleted);
		
		$cnt = 0;
		$result = null;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		while ($stmt->fetch()) {
			$cnt++;
			$result["$theTable"][] = array(
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] => $id,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[1]"] => $trainingtype,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[2]"] => $name,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[3]"] => $lastmodified,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[4]"] => $deleted
			);
		}
		$stmt->close();
		
		
	}
	
	
	public function updateExercises($XmlObjectData, $Ids) {
		
		$error = false;
		$qArray = array();
		$bindParam = new BindParam();
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array
		
		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		$cnt = 0;
		foreach($xml[self::EntityName] as $exercise) {
			
			$query = "";
			$bindParam = new BindParam();
			$qArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
		
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key . ' = ?';
				$value = $exercise["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
		
			$stmt = $this->db->prepare("UPDATE " . self::DB_prefix . self::DB_TABLE . " SET " . $query . " WHERE " .  self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . " = " . $idsArr[$cnt]);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			$cnt++;
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	
	public function deleteExercises($Ids) {
				
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		$mysqli = $this->db;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		foreach ($idsArr as $id) {
			$stmt = $this->db->prepare("DELETE FROM `" . self::DB_prefix . self::DB_TABLE . "` WHERE `" . self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . "` = ?");
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->close();
		}
	}
	
	public function saveExercises($XmlObjectData) {

		$error = false;
		$bindParam = new BindParam();
		$qArray = array();
		$vArray = array();
		$resultSql = "";
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array

		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		foreach($xml[self::EntityName] as $exercise) {
			$query = "";
			$values = "";
			$bindParam = new BindParam();
			$qArray = array();
			$vArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key;
				$vArray[] = '?';
				
				$value = $exercise["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				
				echo "value" . $value;
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
			$values .= implode(', ', $vArray);
			
			$resultSql = "INSERT INTO " . self::DB_prefix . self::DB_TABLE . " (" . $query . ") VALUES (" . $values . ");";
			
			echo $resultSql;
			$stmt = $this->db->prepare($resultSql);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
		
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	
	}
	
	
    // Main method 
    function handleNetworkRequests($requestMethod) {
		
    	
    	// /webservice/exercises/  GET all exercises
        if (strcmp($requestMethod, 'GET') == 0) {
        	$method = $_GET["method"];
        	
        	if (strcmp($method, 'delete') != 0) {
        	
        		$data = null;
        		$dataUUID = $_GET["uuid"];
        		$dataMinor = $_GET["minor"];
        		$dataMajor = $_GET["major"];
        		$dataMAC = $_GET["MAC"];
        		$dataURL = $_GET["address"];
        		
        		
	        	$result = $this->ResolveDevice($dataUUID, $dataMinor, $dataMajor, $dataMAC, $dataURL);
	        	
				if ($result != null) {
					$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
					return true;	
				} else {
					$this->rest->sendResponse(200, $this->rest->xml_encode(""));
					return false;
				}
				
        	} else {
        		
        		$Ids = $_GET["Ids"];
        		
        		if ($Ids != null) {
        		
        			//$ret = $this->deleteExercises($Ids);
        			//$this->rest->sendResponse(200, $ret);
        		}
        	}
        	
        	
		}
		
		// /webservice/exercises/  
		// POST (data contains XML structure) save all exercises
		
		if (strcmp($requestMethod, 'POST') == 0) {
			
			$method = $_POST["method"];
			
			if (strcmp($method, 'update') != 0) {
				 
				$data = null;
				$data = $_POST["data"];
				if ($data != null) {
					$XmlObjectData = $this->rest->xml_decode($data);
					$ret = $this->saveExercises($XmlObjectData);
					
					if ($ret != null) {
						$this->rest->sendResponse(200, $ret);
						return true;
					} else {
						$this->rest->sendResponse(500, "Unable to save data!");
						return false;
					} 
						
				}
					
			} else {
				
				$data = null;
				$data = $_POST["data"];
				$Ids = $_POST["Ids"];
					
				if ($data != null) {
					$XmlObjectData = $this->rest->xml_decode($data);
					$ret = $this->updateExercises($XmlObjectData, $Ids);
						
					if ($ret != null) {
						$this->rest->sendResponse(200, $ret);
						return true;
					} else {
						$this->rest->sendResponse(500, "Unable to update data!");
						return false;
					}
						
				}
			}
			
		}

// 		if ($requestMethod == 'PUT') {
			
// 			$data = null;
// 			$data = $_POST["data"];
// 			$Ids = $_POST["Ids"];
			
// 			if ($data != null) {
// 				$XmlObjectData = $this->rest->xml_decode($data);
// 				$ret = $this->updateExercises($XmlObjectData, $Ids);
			
// 				if ($ret != null) {
// 					$this->rest->sendResponse(200, $ret);
// 					return true;
// 				} else {
// 					$this->rest->sendResponse(500, "Unable to update data!");
// 					return false;
// 				}
					
// 			}
// 		}
		

	}
		
}





$api = new Doombee;
$bledns = new BLEdns;
$bledns->setDb($api->db);
$bledns->setRest($api->rest);
$bledns->handleNetworkRequests($_SERVER['REQUEST_METHOD']);
?>

