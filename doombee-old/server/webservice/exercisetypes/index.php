<?php

include '../main/index.php';


class ExerciseTypes {
	
    private $db;
	private $rest;

	const DB_prefix = "app_";
	const DB_TABLE = "exercise_types";
	
	const EntityName = "exercise_type";
	
	private static $TABLE_ARRAY_STRUCT = array(
			"COLUMN_ID" => "ID",
			"COLUMN_TRAINING_TYPES_ID" => "training_types_id",
			"COLUMN_NAME" => "Name",
			"COLUMN_LAST_MODIFIED" => "last_modified",
			"COLUMN_DELETED" => "del_check"
	);
	private static $TABLE_ARRAY_TYPES = array('i', 'i', 's', 's', 's');
	
	
	public function setDb($db) {
		$this->db = $db;
	}
	
	public function setRest($rest) {
		$this->rest = $rest;
	}
	
	public function getExerciseTypes() {
	
		$curTable = self::DB_prefix . self::DB_TABLE;
		$theTable = self::DB_TABLE;
		
		$stmt = $this->db->prepare('SELECT * FROM ' . $curTable);
		//$stmt->bind_param("sii", $uid, $limitStart, $limit);
		$stmt->execute();
		$stmt->bind_result($id, $trainingtype, $name, $lastModified, $deleted);
	
		$cnt = 0;
		$result = null;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		while ($stmt->fetch()) {
			$cnt++;
			$result["$theTable"][] = array(
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] => $id,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[1]"] => $trainingtype,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[2]"] => $name,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[3]"] => $lastModified,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[4]"] => $deleted
			);

		}
		$stmt->close();
	

		return $result;
	}
	
	
	public function saveExerciseTypes($XmlObjectData) {

		$error = false;
		$bindParam = new BindParam();
		$qArray = array();
		$vArray = array();
		$resultSql = "";
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array

		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		foreach($xml[self::EntityName] as $exercisetype) {
			$query = "";
			$values = "";
			$bindParam = new BindParam();
			$qArray = array();
			$vArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key;
				$vArray[] = '?';
				
				$value = $exercisetype["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				
				echo "value" . $value;
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
			$values .= implode(', ', $vArray);
			
			$resultSql = "INSERT INTO " . self::DB_prefix . self::DB_TABLE . " (" . $query . ") VALUES (" . $values . ");";
			
			echo $resultSql;
			$stmt = $this->db->prepare($resultSql);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
		
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	
	
	public function updateExerciseTypes($XmlObjectData, $Ids) {
		
		$error = false;
		$qArray = array();
		$bindParam = new BindParam();
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array
		
		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		$cnt = 0;
		foreach($xml[self::EntityName] as $exercisetype) {
			
			$query = "";
			$bindParam = new BindParam();
			$qArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
		
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key . ' = ?';
				$value = $exercisetype["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
		
			$stmt = $this->db->prepare("UPDATE " . self::DB_prefix . self::DB_TABLE . " SET " . $query . " WHERE " .  self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . " = " . $idsArr[$cnt]);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			$cnt++;
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	public function deleteExerciseTypes($Ids) {
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		$mysqli = $this->db;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		foreach ($idsArr as $id) {
			$stmt = $this->db->prepare("DELETE FROM `" . self::DB_prefix . self::DB_TABLE . "` WHERE `" . self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . "` = ?");
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->close();
		}
	}
	
	
    // Main method 
    function handleExerciseTypes($requestMethod) {
		
    	// /webservice/exercisetypes/  GET all exercise types
    	if (strcmp($requestMethod, 'GET') == 0) {
    		$method = $_GET["method"];
    		 
    		if (strcmp($method, 'delete') != 0) {
    			 
    			$result = $this->getExerciseTypes();
    	
    			if ($result != null) {
    				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
    				return true;
    			} else {
    				$this->rest->sendResponse(200, $this->rest->xml_encode(""));
    				return false;
    			}
    	
    		} else {
    	
    			$Ids = $_GET["Ids"];
    	
    			if ($Ids != null) {
    	
    				$ret = $this->deleteExerciseTypes($Ids);
    				$this->rest->sendResponse(200, $ret);
    			}
    		}
    		 
    		 
    	}
    	
    	// /webservice/exercises/
    	// POST (data contains XML structure) save all exercises
    	
    	if (strcmp($requestMethod, 'POST') == 0) {
    			
    		$method = $_POST["method"];
    			
    		if (strcmp($method, 'update') != 0) {
    				
    			$data = null;
    			$data = $_POST["data"];
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->saveExerciseTypes($XmlObjectData);
    					
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to save data!");
    					return false;
    				}
    	
    			}
    				
    		} else {
    	
    			$data = null;
    			$data = $_POST["data"];
    			$Ids = $_POST["Ids"];
    				
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->updateExerciseTypes($XmlObjectData, $Ids);
    	
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to update data!");
    					return false;
    				}
    	
    			}
    		}
    			
    	}
    	
//  /*    	if ($requestMethod=='GET') {
//     		$result = $this->getExerciseTypes();
    		 
//     		if ($result != null) {
//     			$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
//     			return true;
//     		} else {
//     			$this->rest->sendResponse(200, $this->rest->xml_encode(""));
//     			return false;
//     		}
//     	}
    	
//     	// /webservice/exercisetypes/
//     	// POST (data contains XML structure) save all exercise types
    	
//     	if ($requestMethod=='POST') {
    			
//     		$data = null;
//     		$data = $_POST["data"];
//     		if ($data != null) {
//     			$XmlObjectData = $this->rest->xml_decode($data);
//     			$ret = $this->saveExerciseTypes($XmlObjectData);
    	
//     			if ($ret != null) {
//     				$this->rest->sendResponse(200, $ret);
//     				return true;
//     			} else {
//     				$this->rest->sendResponse(500, "Unable to save data!");
//     				return false;
//     			}
    				
//     		}
//     	} */
		
	}
}




$api = new MasterSport;
$exerciseTypes = new ExerciseTypes;
$exerciseTypes->setDb($api->db);
$exerciseTypes->setRest($api->rest);
$exerciseTypes->handleExerciseTypes($_SERVER['REQUEST_METHOD']);

?>

