package com.ms.mastersportApp.parsers;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ms.mastersportApp.classes.ClassEnumerator;
import com.ms.mastersportApp.classes.Exercise;
import com.ms.mastersportApp.classes.ExerciseType;
import com.ms.mastersportApp.classes.Group;
import com.ms.mastersportApp.classes.NextTraining;
import com.ms.mastersportApp.classes.PulseExercise;
import com.ms.mastersportApp.classes.PulseRecord;
import com.ms.mastersportApp.classes.PulseUser;
import com.ms.mastersportApp.classes.TrainingPlan;
import com.ms.mastersportApp.classes.TrainingType;
import com.ms.mastersportApp.synchronization.TableStackItem;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


public class XmlParser {

	//private static ArrayList<Category> Categorys;
	public static Bitmap loadPic(String url) {
		
		// the downloaded thumb (none for now!)
		Bitmap pic = null;

		// sub-sampling options
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;

		try {

			// open a connection to the URL
			// Note: pay attention to permissions in the Manifest file!
			URL u = new URL(url);
			URLConnection c = u.openConnection();
			c.connect();
			
			// read data
			BufferedInputStream stream = new BufferedInputStream(c.getInputStream());

			// decode the data, subsampling along the way
			pic = BitmapFactory.decodeStream(stream, null, opts);

			// close the stream
			stream.close();

		} catch (MalformedURLException e) {
			Log.e("Threads03", "malformed url: " + url);
		} catch (IOException e) {
			Log.e("Threads03", "An error has occurred downloading the image: " + url);
		}

		// return the fetched thumb (or null, if error)
		return pic;
	}
	
	private static Document ParseXMLString(String xmlString) {
		
		// fix encoding problem
		xmlString = xmlString.replaceAll("[^\\x20-\\x7e]", "");
		
		DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		Document doc = null;
		
		try {
			InputSource inStream = new InputSource(); 
			inStream.setCharacterStream(new StringReader(xmlString));
			inStream.setEncoding("windows-1252");
			
			db = factory.newDocumentBuilder();
			doc = db.parse(inStream);
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	
		return doc;
	}
	
	// List of methods
	// ParseXMLExerciseTypes
	// ParseXMLExercises
	// ParseXMLGroups
	// ParseXMLTrainingTypesStr
	// ParseXMLTrainingPlans
	
	// ParseXMLPulseRecords
	// ParseXMLPulseUsers
	// ParseXMLPulseExercises
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLExercises(String xmlExercisesStr) {
		 System.out.println("parsing exercises " + xmlExercisesStr + xmlExercisesStr.equals(""));
		 
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();

		List<Exercise> le = ParseXMLExercisesStr(xmlExercisesStr);
			
		for (int i = 0; i < le.size(); i++) {
			Exercise object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	//public static List<ClassEnumerator> ParseXMLGeneric(TableStackItem ts, String className) {
		
	//}
	
	public static List<Exercise> ParseXMLExercisesStr(String xmlExercisesStr) {
		
		Document doc = ParseXMLString(xmlExercisesStr);
		List<Exercise> ListOfExercises = new ArrayList<Exercise>();
		NodeList nodeList = doc.getElementsByTagName("exercise");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				Exercise newExercise = new Exercise();
				
				NodeList idExercise = element.getElementsByTagName("ID");
				if (idExercise.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idExercise.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newExercise.setExercise_id(Integer.valueOf(idValue));
				}
				
				NodeList trainingTId = element.getElementsByTagName("training_types_id");
				if (trainingTId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element trainingElement = (Element) trainingTId.item(0);
					String idTrainingTypeValue = trainingElement.getFirstChild().getNodeValue().trim();
					
					newExercise.setTrainingType_id(Integer.valueOf(idTrainingTypeValue));
				}
				
				NodeList exerciseName = element.getElementsByTagName("Name");
				if (exerciseName.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseNameElement = (Element) exerciseName.item(0);
					String exerciseNameValue = exerciseNameElement.getFirstChild().getNodeValue();
					
					newExercise.setName(exerciseNameValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newExercise.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newExercise.setDelCheck(delcheckValue);
				}
				
			
				ListOfExercises.add(newExercise);
			}
		}
		return ListOfExercises;
	}
	
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLExerciseTypes(String xmlExerciseTypesStr) {
		
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();
		
		List<ExerciseType> le = ParseXMLExerciseTypesStr(xmlExerciseTypesStr);
		
		for (int i = 0; i < le.size(); i++) {
			ExerciseType object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	public static List<ExerciseType> ParseXMLExerciseTypesStr(String xmlExerciseTypesStr) {
		
		Document doc = ParseXMLString(xmlExerciseTypesStr);

		List<ExerciseType> ListOfExerciseTypes = new ArrayList<ExerciseType>();
		NodeList nodeList = doc.getElementsByTagName("exercise_type");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				ExerciseType newExerciseType = new ExerciseType();
				
				NodeList idExerciseType = element.getElementsByTagName("ID");
				if (idExerciseType.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idExerciseType.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
	
					newExerciseType.setExerciseType_id(Integer.valueOf(idValue));
				}
				
				NodeList trainingTId = element.getElementsByTagName("training_types_id");
				if (trainingTId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element trainingElement = (Element) trainingTId.item(0);
					String idTrainingTypeValue = trainingElement.getFirstChild().getNodeValue().trim();
					
					newExerciseType.setTrainingType_id(Integer.valueOf(idTrainingTypeValue));
				}
				
				NodeList exerciseName = element.getElementsByTagName("Name");
				if (exerciseName.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseNameElement = (Element) exerciseName.item(0);
					String exerciseNameValue = exerciseNameElement.getFirstChild().getNodeValue();
					
					newExerciseType.setName(exerciseNameValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newExerciseType.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");
					
					newExerciseType.setDelCheck(delcheckValue);
				}
				ListOfExerciseTypes.add(newExerciseType);
			}
		}
		System.out.println("parsed " + ListOfExerciseTypes.size() + " exercise types");
		return ListOfExerciseTypes;
	}
	
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLGroups(String xmlGroupsStr) {
		
		List<Group> le = ParseXMLGroupsStr(xmlGroupsStr);
		System.out.println("parsed groups" + le.size());
		
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();
		
		for (int i = 0; i < le.size(); i++) {

			Group object = le.get(i);
			System.out.println("enumerating group" + object);
			ClassEnumerator newEnum = object.Enumerate();
			System.out.println("adding enumerator" + newEnum);
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	public static List<Group> ParseXMLGroupsStr(String xmlGroupsStr) {
		
		Document doc = ParseXMLString(xmlGroupsStr);

		List<Group> ListOfGroups = new ArrayList<Group>();
		NodeList nodeList = doc.getElementsByTagName("group");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				Group newGroup = new Group();
				
				NodeList idGroupType = element.getElementsByTagName("ID");
				if (idGroupType.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idGroupType.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newGroup.setGroup_id(Integer.valueOf(idValue));
				}
				
				NodeList groupName = element.getElementsByTagName("Name");
				if (groupName.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element groupNameElement = (Element) groupName.item(0);
					String groupNameValue = groupNameElement.getFirstChild().getNodeValue().trim();
					
					newGroup.setName(groupNameValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newGroup.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newGroup.setDelCheck(delcheckValue);
				}
				ListOfGroups.add(newGroup);
			}
		}
		return ListOfGroups;
	}
	
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLTrainingTypes(String xmlTrainingTypesStr) {
		
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();
		List<TrainingType> le = ParseXMLTrainingTypesStr(xmlTrainingTypesStr);
		
		for (int i = 0; i < le.size(); i++) {
			TrainingType object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	
	public static List<TrainingType> ParseXMLTrainingTypesStr(String xmlTrainingTypesStr) {
		
		Document doc = ParseXMLString(xmlTrainingTypesStr);

		List<TrainingType> ListOfTrainingTypes = new ArrayList<TrainingType>();
		NodeList nodeList = doc.getElementsByTagName("training_type");
	
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				TrainingType newTrainingType = new TrainingType();
				
				NodeList idTrainingType = element.getElementsByTagName("ID");
				if (idTrainingType.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idTrainingType.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newTrainingType.setTrainingType_id(Integer.valueOf(idValue));
				}
				
				NodeList name = element.getElementsByTagName("Name");
				if (name.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element nameElement = (Element)name.item(0);
					String nameValue = nameElement.getFirstChild().getNodeValue().trim();
					
					newTrainingType.setName(nameValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newTrainingType.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newTrainingType.setDelCheck(delcheckValue);
				}
				
				ListOfTrainingTypes.add(newTrainingType);
			}
		}

		  
		return ListOfTrainingTypes;
	}
	
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLTrainingPlans(String xmlTrainingPlansStr) {
		
		System.out.println("parsing training plans...");
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();
		List<TrainingPlan> le = ParseXMLTrainingPlansStr(xmlTrainingPlansStr);

		for (int i = 0; i < le.size(); i++) {
			TrainingPlan object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
		
		return lenum;
	}
	
	
	public static List<TrainingPlan> ParseXMLTrainingPlansStr(String xmlTrainingPlansStr) {
		
		Document doc = ParseXMLString(xmlTrainingPlansStr);

		List<TrainingPlan> ListOfTrainingPlans = new ArrayList<TrainingPlan>();
		NodeList nodeList = doc.getElementsByTagName("training_plan");

		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				TrainingPlan newTrainingPlan = new TrainingPlan();
			
				NodeList idTrainingPlanId = element.getElementsByTagName("training_plan_id");
				if (idTrainingPlanId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idTrainingPlan = (Element)idTrainingPlanId.item(0);
					String idValue = idTrainingPlan.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setTrainingPlan_id(Integer.valueOf(idValue));
				}

				NodeList idTrainingType = element.getElementsByTagName("training_types_id");
				if (idTrainingType.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idTrainingType.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setTrainingType_id(Integer.valueOf(idValue));
				}
				
				NodeList groupId = element.getElementsByTagName("groups_id");
				if (groupId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element groupIdElement = (Element) groupId.item(0);
					String groupIdValue = groupIdElement.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setGroup_id(Integer.valueOf(groupIdValue));
				}
				
				NodeList sequenceId = element.getElementsByTagName("sequence");
				if (sequenceId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element sequenceIdElement = (Element) sequenceId.item(0);
					String sequenceIdValue = sequenceIdElement.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setSeqence(Integer.valueOf(sequenceIdValue));
				}
				
				NodeList exerciseId = element.getElementsByTagName("exercises_id");
				if (exerciseId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseIdElement = (Element) exerciseId.item(0);
					String exerciseIdValue = exerciseIdElement.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setExerciseId(Integer.valueOf(exerciseIdValue));
				}
				
				NodeList exerciseType1 = element.getElementsByTagName("exercise_type_id_1");
				if (exerciseType1.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseType1Element = (Element) exerciseType1.item(0);
					String exerciseType1Value = exerciseType1Element.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setExercise_type_id_1(Integer.valueOf(exerciseType1Value));
				}
				
				NodeList exerciseType2 = element.getElementsByTagName("exercise_type_id_2");
				if (exerciseType2.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseType2Element = (Element) exerciseType2.item(0);
					String exerciseType2Value = exerciseType2Element.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setExercise_type_id_2(Integer.valueOf(exerciseType2Value));
				}
				
				NodeList exerciseType3 = element.getElementsByTagName("exercise_type_id_3");
				if (exerciseType3.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseType3Element = (Element) exerciseType3.item(0);
					String exerciseType3Value = exerciseType3Element.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setExercise_type_id_3(Integer.valueOf(exerciseType3Value));
				}
				
				NodeList exerciseType4 = element.getElementsByTagName("exercise_type_id_4");
				if (exerciseType4.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element exerciseType4Element = (Element) exerciseType4.item(0);
					String exerciseType4Value = exerciseType4Element.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setExercise_type_id_4(Integer.valueOf(exerciseType4Value));
				}
				
				NodeList time = element.getElementsByTagName("time");
				if (time.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element timeElement = (Element) time.item(0);
					String timeValue = timeElement.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setTime(Integer.valueOf(timeValue));
				}
				
				NodeList splitTime = element.getElementsByTagName("split_time");
				if (splitTime.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element splitTimeElement = (Element) splitTime.item(0);
					String splitTimeValue = splitTimeElement.getFirstChild().getNodeValue().trim();
					
					newTrainingPlan.setSplitTime(Integer.valueOf(splitTimeValue));
				}
				
				NodeList comment = element.getElementsByTagName("comment");
				if (comment.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element commentElement = (Element) comment.item(0);
					
					String commentValue;
					if (commentElement.getFirstChild() != null) {
						commentValue = commentElement.getFirstChild().getNodeValue().trim();
					} else {
						commentValue = "";
					}
					
					newTrainingPlan.setComment(commentValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					System.out.println("parse training plan " + lastModifiedValue);
					newTrainingPlan.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newTrainingPlan.setDelCheck(delcheckValue);
				}
				ListOfTrainingPlans.add(newTrainingPlan);
				
			}
		}
		
		return ListOfTrainingPlans;
	}
	

	
	// ParseXMLPulseRecords
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLPulseRecords(String xmlPulseRecordsStr) {
		 System.out.println("parsing pulse records " + xmlPulseRecordsStr + xmlPulseRecordsStr.equals(""));
		 
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();

		List<PulseRecord> le = ParseXMLPulseRecordsStr(xmlPulseRecordsStr);
			
		for (int i = 0; i < le.size(); i++) {
			PulseRecord object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	
	public static List<PulseRecord> ParseXMLPulseRecordsStr(String xmlExercisesStr) {
		
		Document doc = ParseXMLString(xmlExercisesStr);
		List<PulseRecord> ListOfPulseRecords = new ArrayList<PulseRecord>();
		NodeList nodeList = doc.getElementsByTagName("pulse_record");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				PulseRecord newPulseRecord = new PulseRecord();
				
				NodeList idRecord = element.getElementsByTagName("ID");
				if (idRecord.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idRecord.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulseRecord_id(Integer.valueOf(idValue));
				}
				
				NodeList pulseUserId = element.getElementsByTagName("pulse_user_id");
				if (pulseUserId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element pulseUserIdElement = (Element) pulseUserId.item(0);
					String pulseUserIdValue = pulseUserIdElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulseUser_id(Integer.valueOf(pulseUserIdValue));
				}
				
				NodeList pulseExerciseId = element.getElementsByTagName("pulse_exercise_id");
				if (pulseExerciseId.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element pulseExerciseIdElement = (Element) pulseExerciseId.item(0);
					String pulseExerciseIdValue = pulseExerciseIdElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulseExercise_id(Integer.valueOf(pulseExerciseIdValue));
				}
				
				NodeList data = element.getElementsByTagName("data");
				if (data.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element dataElement = (Element) data.item(0);
					
					String dataValue = "";
					
					if (dataElement.getFirstChild() != null) {
						dataValue = dataElement.getFirstChild().getNodeValue().trim();
					}
					newPulseRecord.setData(dataValue);
				}
				
				NodeList datafile = element.getElementsByTagName("data_file");
				if (datafile.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element datafileElement = (Element) datafile.item(0);
					
					String datafileValue = ""; 
					if (datafileElement.getFirstChild() != null) {
						datafileValue = datafileElement.getFirstChild().getNodeValue().trim();
					}
					newPulseRecord.setData_file(datafileValue);
				}
				
				NodeList dateofrecord = element.getElementsByTagName("record_date");
				if (dateofrecord.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element dateofrecordElement = (Element) dateofrecord.item(0);
					String dateofrecordValue = dateofrecordElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setDate_of_record(dateofrecordValue);
				}
				
				NodeList pulseAverage = element.getElementsByTagName("record_pulse_average");
				if (pulseAverage.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element pulseAverageElement = (Element) pulseAverage.item(0);
					String pulseAverageValue = pulseAverageElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulse_average_for_record(Integer.valueOf(pulseAverageValue));
				}
				
				
				NodeList pulseMax = element.getElementsByTagName("record_pulse_max");
				if (pulseMax.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element pulseMaxElement = (Element) pulseMax.item(0);
					String pulseMaxValue = pulseMaxElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulse_max_for_record(Integer.valueOf(pulseMaxValue));
				}
				
				NodeList pulseStart = element.getElementsByTagName("record_pulse_start");
				if (pulseStart.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element pulseStartElement = (Element) pulseStart.item(0);
					String pulseStartValue = pulseStartElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulse_start_for_record(Integer.valueOf(pulseStartValue));
				}
				
				NodeList pulseRecovery = element.getElementsByTagName("record_pulse_recovery");
				if (pulseRecovery.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element pulseRecoveryElement = (Element) pulseRecovery.item(0);
					String pulseRecoveryValue = pulseRecoveryElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setPulse_recovery_for_record(Integer.valueOf(pulseRecoveryValue));
				}
				
				NodeList comments = element.getElementsByTagName("record_comment");
				if (comments.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element commentsElement = (Element) comments.item(0);
					String commentsValue = "";
					if (commentsElement.getFirstChild() != null) {
						commentsValue = commentsElement.getFirstChild().getNodeValue().trim();
					}
					newPulseRecord.setComments(commentsValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newPulseRecord.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newPulseRecord.setDelCheck(delcheckValue);
				}
				
				ListOfPulseRecords.add(newPulseRecord);
			}
		}
		return ListOfPulseRecords;
	}
	
	
	// ParseXMLPulseUsers

	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLPulseUsers(String xmlPulseUsersStr) {
		 System.out.println("parsing pulse users " + xmlPulseUsersStr + xmlPulseUsersStr.equals(""));
		 
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();

		List<PulseUser> le = ParseXMLPulseUsersStr(xmlPulseUsersStr);
		System.out.println("Pulse users parsed");
		for (int i = 0; i < le.size(); i++) {
			PulseUser object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	//public static List<ClassEnumerator> ParseXMLGeneric(TableStackItem ts, String className) {
		
	//}
	
	public static List<PulseUser> ParseXMLPulseUsersStr(String xmlPulseUsersStr) {
		
		Document doc = ParseXMLString(xmlPulseUsersStr);
		List<PulseUser> ListOfPulseUsers = new ArrayList<PulseUser>();
		NodeList nodeList = doc.getElementsByTagName("pulse_user");
		
		System.out.println("Parse pulse users " + nodeList.getLength());
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				PulseUser newPulseUser = new PulseUser();
				
				NodeList idPulseUser = element.getElementsByTagName("ID");
				if (idPulseUser.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idPulseUser.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newPulseUser.setPulse_User_Id(Integer.valueOf(idValue));
				}
				
				NodeList name = element.getElementsByTagName("name");
				if (name.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element nameElement = (Element) name.item(0);
					String nameValue = nameElement.getFirstChild().getNodeValue().trim();
					
					newPulseUser.setName(nameValue);
				}
				
				NodeList surname = element.getElementsByTagName("surname");
				if (surname.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element surnameElement = (Element) surname.item(0);
					String surnameValue = surnameElement.getFirstChild().getNodeValue().trim();
					
					newPulseUser.setSurname(surnameValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newPulseUser.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");

					newPulseUser.setDelCheck(delcheckValue);
				}
				
			
				ListOfPulseUsers.add(newPulseUser);
			}
		}
		
		return ListOfPulseUsers;
	}
	
	
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLPulseExercises(String xmlPulseExercisesStr) {
		 System.out.println("parsing pulse exercises " + xmlPulseExercisesStr + xmlPulseExercisesStr.equals(""));
		 
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();

		List<PulseExercise> le = ParseXMLPulseExercisesStr(xmlPulseExercisesStr);
			
		for (int i = 0; i < le.size(); i++) {
			PulseExercise object = le.get(i);
			ClassEnumerator newEnum = object.Enumerate();
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	
	public static List<PulseExercise> ParseXMLPulseExercisesStr(String xmlPulseExercisesStr) {
		
		Document doc = ParseXMLString(xmlPulseExercisesStr);
		List<PulseExercise> ListOfPulseExercises = new ArrayList<PulseExercise>();
		NodeList nodeList = doc.getElementsByTagName("pulse_exercise");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				PulseExercise newPulseExercise = new PulseExercise();
				
				NodeList idPulseUser = element.getElementsByTagName("ID");
				if (idPulseUser.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idPulseUser.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newPulseExercise.setExercise_id(Integer.valueOf(idValue));
				}
				
				NodeList name = element.getElementsByTagName("name");
				if (name.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element nameElement = (Element) name.item(0);
					String nameValue = nameElement.getFirstChild().getNodeValue().trim();
					
					newPulseExercise.setName(nameValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newPulseExercise.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newPulseExercise.setDelCheck(delcheckValue);
				}
				
			
				ListOfPulseExercises.add(newPulseExercise);
			}
		}
		
		return ListOfPulseExercises;
	}

	
	
	
	
	// overload for syncronization
	public static List<ClassEnumerator> ParseXMLNextTraining(String xmlNextTrainingStr) {
		 System.out.println("parsing next training " + xmlNextTrainingStr + xmlNextTrainingStr.equals(""));
		 
		List<NextTraining> le = ParseXMLNextTrainingStr(xmlNextTrainingStr);
		System.out.println("parsed next training" + le.size());
		
		List<ClassEnumerator> lenum = new ArrayList<ClassEnumerator>();
		
		for (int i = 0; i < le.size(); i++) {

			NextTraining object = le.get(i);
			System.out.println("enumerating group" + object);
			ClassEnumerator newEnum = object.Enumerate();
			System.out.println("adding enumerator" + newEnum);
			lenum.add(newEnum);
		}
			
		return lenum;
	}
	
	public static List<NextTraining> ParseXMLNextTrainingStr(String xmlNextTrainingStr) {
		
		Document doc = ParseXMLString(xmlNextTrainingStr);

		List<NextTraining> ListOfNextTraining = new ArrayList<NextTraining>();
		NodeList nodeList = doc.getElementsByTagName("next_training");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				NextTraining newNextTraining = new NextTraining();
				
				NodeList idnexttraining = element.getElementsByTagName("ID");
				if (idnexttraining.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idElement = (Element)idnexttraining.item(0);
					String idValue = idElement.getFirstChild().getNodeValue().trim();
					
					newNextTraining.setNextTrainingId(Integer.valueOf(idValue));
				}
				
				NodeList groupsid = element.getElementsByTagName("groups_id");
				if (groupsid.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idgroupsElement = (Element)groupsid.item(0);
					String idgroupsValue = idgroupsElement.getFirstChild().getNodeValue().trim();
					
					newNextTraining.setGroupId(Integer.valueOf(idgroupsValue));
				}
				
				NodeList trainingtypesid = element.getElementsByTagName("training_types_id");
				if (trainingtypesid.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element idtrainingtypeElement = (Element)trainingtypesid.item(0);
					String idtrainingtypeValue = idtrainingtypeElement.getFirstChild().getNodeValue().trim();
					
					newNextTraining.setTrainingTypeId(Integer.valueOf(idtrainingtypeValue));
				}
				
				NodeList sequence = element.getElementsByTagName("sequence");
				if (sequence.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element sequenceElement = (Element) sequence.item(0);
					String sequenceValue = sequenceElement.getFirstChild().getNodeValue().trim();
					
					newNextTraining.setSequence(sequenceValue);
				}
				
				NodeList lastmodified = element.getElementsByTagName("last_modified");
				if (lastmodified.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element lastmodifiedElement = (Element) lastmodified.item(0);
					String lastModifiedValue = lastmodifiedElement.getFirstChild().getNodeValue().trim();
					
					newNextTraining.setLastModified(lastModifiedValue);
				}
				
				NodeList delcheck = element.getElementsByTagName("del_check");
				if (delcheck.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element delcheckElement = (Element) delcheck.item(0);
					Boolean delcheckValue = delcheckElement.getFirstChild().getNodeValue().trim().equalsIgnoreCase("1");;
					
					newNextTraining.setDelCheck(delcheckValue);
				}
				ListOfNextTraining.add(newNextTraining);
			}
		}
		
		return ListOfNextTraining;
	}
	
	
	
	
}
