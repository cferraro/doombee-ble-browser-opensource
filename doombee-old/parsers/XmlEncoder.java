package com.ms.mastersportApp.parsers;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ms.mastersportApp.classes.ClassEnumerator;
import com.ms.mastersportApp.synchronization.TableStackItem;

public class XmlEncoder {

	public static String GenericXMLEncodeList(List<ClassEnumerator> ce, String tableName) {
		
		
		String rootStr = tableName;
		String childStr = tableName;
		if ( rootStr.substring(rootStr.length() - 1).compareTo("s") == 0) {
			childStr = rootStr.substring(0, rootStr.length() - 1);
		}

		String strInputXML = "";
		
        try{
            //Create instance of DocumentBuilderFactory
            DocumentBuilderFactory Docfactory = 
                    DocumentBuilderFactory.newInstance();
             //Get the DocumentBuilder
            DocumentBuilder parser = Docfactory.newDocumentBuilder();
             //Create blank DOM Document
            Document doc=parser.newDocument();
            //create the root element
            Element root=doc.createElement(rootStr);
            //all it to the xml tree
            doc.appendChild(root);
            //creat child element
            
            
            
            // create ROW
            for (int i = 0; i < ce.size(); i++) {
                Element childelement=doc.createElement(childStr);
                
                // add COLUMNS
                ClassEnumerator row = ce.get(i);
                List<String> columnList = row.getNames();
                List<Object> rowValues = row.getValues();
                
                for (int j = 0; j < columnList.size(); j++) {
                	
                	String curColumnName = columnList.get(j);
                	Object curValue = rowValues.get(j);
                	Element column = doc.createElement(curColumnName);
                	column.setTextContent(curValue.toString());
                	
                	childelement.appendChild(column);
                }
                root.appendChild(childelement);
            }
            


            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            Properties outFormat = new Properties();
            outFormat.setProperty(OutputKeys.INDENT, "yes");
            outFormat.setProperty(OutputKeys.METHOD, "xml");
            outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            outFormat.setProperty(OutputKeys.VERSION, "1.0");
            outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperties(outFormat);
            DOMSource domSource = new DOMSource(doc.getDocumentElement());
            OutputStream output = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(output);
            transformer.transform(domSource, result);
            strInputXML = output.toString(); //Storing into a string
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
		
		return strInputXML;
	}
}
