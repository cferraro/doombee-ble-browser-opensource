# README #

### What is this repository for? ###

* This Repository is intended to provide a solid development platform and browser for all possible Ibeacons/BLE Devices.
* Version 0.1.8 for Android 4.3+ (Repository started the 25/06/2015)
* I'm constantly searching for people, willing to help us in development of this fantastic browser.

NEWS: 28/07/2015
Quatja has joined our Opensource team. Welcome

- Added Cloud support and Basic DNS resolver.
- Added 404 Error page.
- First connection tests to Ibeacons.
- Added Read functionality and base CRUD classes
- Created Base Extensible Library for Beacons. (Will include Google Eddystone)

What can You do ?

- [DOWNLOAD](https://bitbucket.org/cferraro/doombee-ble-browser-opensource/raw/5597bc63549bfe5b5abff15e75c6ba90f8522707/doombee-studio/app/build/outputs/apk/app-debug.apk)
HERE A BINARY TEST FILE.

- Code for this project.

- [VISIT OUR WEBSITE AND DONATE FOR THIS PROJECT :-)] (http://www.doombee.com)

Leave me a feedback too at:
info@doombee.com


or skype me at:
sunclaude