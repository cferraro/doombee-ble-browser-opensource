<?php
include 'restutils.php';


class Top10 {
    private $db;
	private $rest;
	
    // Constructor - open DB connection
    function __construct() {
        $this->db = new mysqli('localhost', 'root', '', 'dressup');
        $this->db->autocommit(FALSE);
		$this->rest = new RestUtils();
    }
	
    // Destructor - close DB connection
    function __destruct() {
        $this->db->close();
    }
 
    // Main method 
    function Top10($requestMethod) {
		
		// Get top 10
        if ($requestMethod=='GET') {

			if (isset($_GET["session_id"])) {
				
				// HERE YOUR CODE
				
				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
				return true;
			} else {
							
				$this->rest->sendResponse(500, 'Session not set');
				return false;
			}
		}
		
	
		// SET TOP 10 item
		if ($requestMethod=='POST') {
			
			if (isset($_POST["score"]) && isset($_POST["session_id"])) {
				
		// Check for required parameters
		if (isset($_POST["session_id"]) && isset($_POST["code"]) && isset($_POST["device_id"])) {
 
			// Put parameters into local variables
			$rw_app_id = $_POST["rw_app_id"];
			$code = $_POST["code"];
			$device_id = $_POST["device_id"];
	 
			// Look up code in database
			$user_id = 0;
			$stmt = $this->db->prepare('SELECT id, unlock_code, uses_remaining FROM rw_promo_code WHERE rw_app_id=? AND code=? LIMIT ?, ?');
			$stmt->bind_param("ii", $rw_app_id, $code);
			$stmt->execute();
			$stmt->bind_result($id, $unlock_code, $uses_remaining);
			while ($stmt->fetch()) {
				break;
			}
			$stmt->close();
	 
			// Bail if code doesn't exist
			if ($id <= 0) {
				sendResponse(400, 'Invalid code');
				return false;
			}
	 
			// Bail if code already used		
			if ($uses_remaining <= 0) {
				sendResponse(403, 'Code already used');
				return false;
			}	
	 
			// Check to see if this device already redeemed	
			$stmt = $this->db->prepare('SELECT id FROM rw_promo_code_redeemed WHERE device_id=? AND rw_promo_code_id=?');
			$stmt->bind_param("si", $device_id, $id);
			$stmt->execute();
			$stmt->bind_result($redeemed_id);
			while ($stmt->fetch()) {
				break;
			}
			$stmt->close();
	 
			// Bail if code already redeemed
			if ($redeemed_id > 0) {
				sendResponse(403, 'Code already used');
				return false;
			}
	 
			// Add tracking of redemption
			$stmt = $this->db->prepare("INSERT INTO rw_promo_code_redeemed (rw_promo_code_id, device_id) VALUES (?, ?)");
			$stmt->bind_param("is", $id, $device_id);
			$stmt->execute();
			$stmt->close();
	 
	 
			// Decrement use of code
			$this->db->query("UPDATE rw_promo_code SET uses_remaining=uses_remaining-1 WHERE id=$id");
			$this->db->commit();
	  $mysqli->affected_rows
			// Return unlock code, encoded with JSON
			$result = array(
				"unlock_code" => $unlock_code,
			);
			sendResponse(200, json_encode($result));
			return true;
		}
		sendResponse(400, 'Invalid request');
		return false;
	
    }
			}
		}

	}
}


$api = new Top10;
$api->Top10($_SERVER['REQUEST_METHOD']);
?>

