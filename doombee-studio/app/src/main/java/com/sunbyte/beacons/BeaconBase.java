package com.sunbyte.beacons;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class BeaconBase {

    ArrayList<? extends BeaconBase> beaconTypesList = new ArrayList<BeaconBase>();
    byte[] data;
    ArrayList<BeaconFrame> lbeaconframe;

    public void loadBeaconModels() {

        // beacon manifest. Add new beacon types here
        List<BeaconBase> beaconTypesList = new ArrayList<BeaconBase>();

        beaconTypesList.add(new iBeacon());
        beaconTypesList.add(new Eddystone());

    }

    public BeaconBase getBeaconChild() {

        BeaconBase ret = null;

        for (int i=0; i < beaconTypesList.size(); i++) {
           if (beaconTypesList.get(i).getBeaconInstance() != null) {
               ret = beaconTypesList.get(i).getBeaconInstance();
               break;
           }
        }

        return ret;
    }

    public BeaconBase getBeaconInstance() {
        return null;
    }

    // constructor
    BeaconBase() {
        this.lbeaconframe = new ArrayList<BeaconFrame>();
    }

    public void setBeaconFrameList(ArrayList<BeaconFrame> list) {
        this.lbeaconframe = list;
    }

    public ArrayList<BeaconFrame>  getBeaconFrameList() {
        return this.lbeaconframe;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() { return this.data; }




}
