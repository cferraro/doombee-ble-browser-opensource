package com.sunbyte.beacons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class BeaconParser {

    // constructor
    BeaconParser() {

    }


    public BeaconBase parse(byte[] scanRecord) {

        BeaconBase beacon = new BeaconBase();
        beacon.setData(scanRecord);

        if (scanRecord == null) return null;

        // List of AD structures.
        ArrayList<BeaconFrame> list = new ArrayList<BeaconFrame>();

        // If parameters are wrong.
        //if (offset < 0 || length < 0 || payload.length <= offset)
        //{
            // Simply return an empty list without throwing an exception.
        //    return list;
        //}

        // The index to terminate the valid range.
        //int end = Math.min(length, payload.length);

        for (int i = 0; i < scanRecord.length; )
        {
            // The first octet represents 'length'.
            int framelen = scanRecord[i] & 0xFF;

            // If the length is zero.
            if (framelen == 0) break;

            if ((scanRecord.length - i - 1) < framelen)
            {
                // Remaining bytes are insufficient.
                break;
            }

            // The second octet represents 'type'.
            int frametype = scanRecord[i + 1] & 0xFF;

            // The AD data.
            byte[] framedata = Arrays.copyOfRange(scanRecord, i + 2, i + framelen + 1);

            // Create a beacon frame
            BeaconFrame bf = new BeaconFrame(framedata, framelen, frametype);

            list.add(bf);

            // next frame
            i += 1 + framelen;
        }

        beacon.setBeaconFrameList(list);
        beacon.loadBeaconModels();
        if (beacon.getBeaconChild() instanceof iBeacon) {
            iBeacon ib = (iBeacon) beacon.getBeaconChild();

        }

        return beacon;
    }

}
