package com.sunbyte.beacons;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class BeaconFrame {

    byte[] data;
    int length;
    int type;

    // constructor
    public BeaconFrame(byte[] data, int length, int type) {
        this.setData(data);
        this.length = length;
        this.type = type;
    }

    public void setParentType(int parentType) {

    }

    public void setData(byte[] data) {
        this.data = data;
    }

}
