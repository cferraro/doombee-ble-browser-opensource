package com.sunbyte.doombee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sunbyte.classes.BLECharacteristic;
import com.sunbyte.classes.BLEService;
import com.sunbyte.classes.CharacteristicCRUD;
import com.sunbyte.doombee.util.SystemUiHider;
import com.sunbyte.doombee.ui.CustomDialog;
import com.sunbyte.services.BluetoothLeService;
import com.sunbyte.services.SampleGattAttributes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class ChromiumActivity extends Activity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;

	
	private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    
    private List<BLEService> mGattServicesList;
    
    
    
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    
    private boolean mConnected = false;
    private String mDeviceAddress;
    private String mDeviceName;
    
    final Context ctx = this;
    
    
    CharacteristicCRUD BLECRUD;
    
    BluetoothGattCharacteristic characteristicToWrite;
    
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	System.out.println("service connection " + mBluetoothLeService);
        	
        	mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            
            if (!mBluetoothLeService.initialize()) {
                System.out.println("Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };
    
    
	// WEBSOCKETS

    URI uri;
    try {
        uri = new URI("ws://websockethost:8080");
    } catch (URISyntaxException e) {
        e.printStackTrace();
        return;
    }

    mWebSocketClient = new WebSocketClient(uri) {
        @Override
        public void onOpen(ServerHandshake serverHandshake) {
            Log.i("Websocket", "Opened");
            mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
        }

        @Override
        public void onMessage(String s) {
            final String message = s;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView textView = (TextView)findViewById(R.id.messages);
                    textView.setText(textView.getText() + "\n" + message);
                }
            });
        }

        @Override
        public void onClose(int i, String s, boolean b) {
            Log.i("Websocket", "Closed " + s);
        }

        @Override
        public void onError(Exception e) {
            Log.i("Websocket", "Error " + e.getMessage());
        }
    };
    mWebSocketClient.connect();

    // END WEBSOCKETS

	
	View.OnClickListener onClickSettings = new View.OnClickListener() {
	    public void onClick(View v) {
	    
	    	
//	    	final Handler handler = new Handler(); 
//	    	Runnable runnable = new Runnable() { 
//
//	    	    @Override 
//	    	    public void run() { 
//	    	        try{
//	    	            //do your code here
//	    	            //also call the same runnable 
//	    	            handler.postDelayed(this, 1000);
//	    	        }
//	    	        catch (Exception e) {
//	    	            // TODO: handle exception
//	    	        }
//	    	        finally{
//	    	            //also call the same runnable 
//	    	            handler.postDelayed(this, 1000); 
//	    	        }
//	    	    } 
//	    	}; 
//	    	handler.postDelayed(runnable, 1000); 
	    	
	    	
	    	
	    	if (mBluetoothLeService != null && BLECRUD != null) {
	    		 //mBluetoothLeService.connect(mDeviceAddress);
	    	
	    	
		    	// open settings dialog
		    	Dialog dialog = null;
		    	CustomDialog.Builder customBuilder = new
	                   CustomDialog.Builder(ChromiumActivity.this);
	                   customBuilder.setTitle("Device Services")
	                           .setMessage("Custom body")
	                           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	                                   public void onClick(DialogInterface dialog, int which) {
	                                	     dialog.dismiss();
	                                   }
	                           })
	                           .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
	                                   public void onClick(DialogInterface dialog, int which) {
	                                           dialog.dismiss();
	                                   }
	                           });
	            customBuilder.setServicesList(mGattServicesList);
	            customBuilder.setBLECRUDDevice(BLECRUD);
	           
	            dialog = customBuilder.create();
	            dialog = customBuilder.createTableLayout();
	
	            dialog.show();
	            
	    	}
	    	
	    	
	    	//characteristicToWrite.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
	        //mBluetoothLeService.writeCharacteristic(characteristicToWrite, "h");
            
	    }
	  };
	  
	  
	  
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_chromium);

		mGattServicesList = new ArrayList<BLEService>();
		
		WebView wview = (WebView) findViewById(R.id.webview);
		
		Intent intent = getIntent();

		String URItoLoad = intent.getStringExtra("ResolvedURI");
		String DeviceName = intent.getStringExtra("DeviceName");
		String DeviceAddress = intent.getStringExtra("DeviceAddress");
		String DeviceUUID = intent.getStringExtra("DeviceUUID");
		String DeviceMajor = intent.getStringExtra("DeviceMajor");
		String DeviceMinor = intent.getStringExtra("DeviceMinor");
		
		System.out.println("LOADING Site " + URItoLoad);
		wview.getSettings().setJavaScriptEnabled(true);
		
		if (!URItoLoad.isEmpty()) {
			wview.loadUrl(URItoLoad);
		} else {
			
			String Data404 = "<!DOCTYPE html><head> <meta http-equiv=\"Content-Type\" " +
"content=\"text/html; charset=utf-8\"> <html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1250\">"+
"<title></title></head>"+
"<body id=\"body\">"+
"<h1>BLE Error 404</h1><ul>"+
"<li><b>Device Name:</b> " + DeviceName +"</li>"+
"<li><b>Device MAC Address:</b> " + DeviceAddress +"</li>"+
"<li><b>Device UUID:</b> " + DeviceUUID +"</li>"+
"<li><b>Device Minor:</b> " + DeviceMinor +"</li>"+
"<li><b>Device Major:</b> " + DeviceMajor +"</li>"+
"</ul></body></html>";
			
			System.out.println("Loade webview data");
			wview.getSettings().setDefaultTextEncodingName("utf-8");
			wview.loadDataWithBaseURL(null, Data404, "text/html", "UTF-8", null);
		}
		wview.setWebViewClient(new WebViewClient());
		
		
		
		Button settingsBtn = (Button)findViewById(R.id.btn_settings);
		settingsBtn.setOnClickListener(onClickSettings);
		
		System.out.println("Device gatt " + DeviceName );
		System.out.println("Address gatt " + DeviceAddress);
        mDeviceName = DeviceName;
        mDeviceAddress = DeviceAddress;
        

        if (mDeviceAddress != null) {
        	System.out.println("BIND service");
        	Intent gattServiceIntent = new Intent(getApplicationContext(), BluetoothLeService.class);
        	getApplicationContext().bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        }
        
	    //final Intent intent = getIntent();

	    
	        
	        
		
		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
		//findViewById(R.id.dummy_button).setOnTouchListener(
		//		mDelayHideTouchListener);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.

	}


	// Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                // Show all the supported services and characteristics on the user interface.
            	loadGattServices(mBluetoothLeService.getSupportedGattServices());

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
            	
            	if (BLECRUD != null) {

                    String DATA = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
                    String RelatedUUID = intent.getStringExtra(BluetoothLeService.EXTRA_UUID);

                    BLECRUD.setNewData(DATA, RelatedUUID);
                    BLECRUD.setAsyncInputIsReady(true);
            	}

            }
        }
    };
    
    
    
    /*
    * Load the Gatt services and characteristics
    */
    private void loadGattServices(List<BluetoothGattService> gattServices) {
    	
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = "UNKNOWN SERVICE";
        String unknownCharaString = "UNKNOWN CHARACTERISTIC";
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            
        	HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            
            // adding service to list
            BLEService newbservice = new BLEService();
            newbservice.setUUID(uuid);
            newbservice.setName(SampleGattAttributes.lookup(uuid, unknownServiceString));
            newbservice.setgattService(gattService);
                        
            gattServiceData.add(currentServiceData);
            
            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();
            ArrayList<BLECharacteristic> listOfCharacteristics = new ArrayList<BLECharacteristic>();
            
            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                
                
                System.out.println("PERMISSION IS" + currentServiceData.size() + " " + charas.size() + " "  + gattCharacteristic.getPermissions());
            	//System.out.println("PROPERTIES" + currentServiceData.size() + " " + charas.size() + " " + isCharacteristicWriteable(gattCharacteristic));
            	
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
                
                BLECharacteristic newbcharacteristic = new BLECharacteristic();
                newbcharacteristic.setgattCharacteristic(gattCharacteristic);
                newbcharacteristic.setUUID(uuid);
                newbcharacteristic.setName(SampleGattAttributes.lookup(uuid, unknownCharaString));
                listOfCharacteristics.add(newbcharacteristic);
            }
            
            
            newbservice.setListOfCharacteristics(listOfCharacteristics);
            mGattServicesList.add(newbservice);
            
            //mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }
        
        
        BLECRUD = new CharacteristicCRUD(mBluetoothLeService);

        // Read Characteristics

        System.out.println("GATTCHARACTERISTICDATA " + gattCharacteristicData);
        
        System.out.println("GATTSERVICEDATA" + gattServiceData);

        // READ
        //0   0-2
//	        	System.out.println("SIZE " + mGattCharacteristics.size());
//	        	
//	        	final BluetoothGattCharacteristic characteristic =
//		                mGattCharacteristics.get(0).get(0);
//		        final int charaProp = characteristic.getProperties();
//
//	        	System.out.println("PROPERTIES" + isCharacteristicWriteable(characteristic));
//	        	
//		        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
//
//		        	System.out.println("PROPERTY READ");
//		        	if (mNotifyCharacteristic != null) {
//		                mBluetoothLeService.setCharacteristicNotification(
//	                        mNotifyCharacteristic, true);
//		                mNotifyCharacteristic = null;
//		            }
//		        	
//		        	System.out.println("PROPERTY IS " + characteristic.getProperties());
//		        	
//		        	System.out.println("PERMISSION IS " + characteristic.getPermissions());
//		        	
//		            mBluetoothLeService.readCharacteristic(characteristic);
//		        }

		        
		        //WRITE
//	        	characteristicToWrite =
//		                mGattCharacteristics.get(0).get(0);
//	        	
//	        	System.out.println("NOTIFY " + characteristicToWrite.PROPERTY_NOTIFY);
//	        	
//	        	final int charaProp = characteristicToWrite.getProperties();
//	        	System.out.println("PROPERTIES" + isCharacteristicWriteable(characteristicToWrite));
//                if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
//                	
//                	System.out.println("PROPERTY NOTIFY" + (charaProp | BluetoothGattCharacteristic.PROPERTY_WRITE) + " " + (charaProp | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE));
//                    mNotifyCharacteristic = characteristicToWrite;
//                    mBluetoothLeService.setCharacteristicNotification(
//                    		characteristicToWrite, true);
//                }
//	        	
//    	    	characteristicToWrite.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
//    	        mBluetoothLeService.writeCharacteristic(characteristicToWrite, "eNrtUEFuwjAQ/Eou4UbSJkRVK0VVEULqBVARFVfH6yWONyaKQ9PwLL7Ax7rB6h84IFnrnfHMrLxl1zXuLY7hKH+06lUbDcKC+o3ksY7fTy3lg5iCdmbanArSMkw/wmTJh8rdri/2m9UT2dn+m5nUbYazHL4+abWd9+tmWC9nAsJk3m73fZguJlbUKl+ITmqjk+ism4nMs0w9v76ILMkgAQTnABEOgMYeDFkIkAkgtA4MYeWLC5AcVJ7yL5YRgcPrhfXGwfWCtb/4M32g+lYBoCM2ExZs5lzL7lFNwYg50fiYW+aI/6eO0oDDaZRbHlNx5fYm9hZWVoRgy8c6H+u823X+Ae2AgAQ=");

		        
               
        
    }
    
    

    private void displayData(String data) {
        if (data != null) {
           System.out.println("GATT DATA " + data);
        }
    }
    
   @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
        }
    }
   
   private static IntentFilter makeGattUpdateIntentFilter() {
       final IntentFilter intentFilter = new IntentFilter();
       intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
       intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
       intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
       intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
       return intentFilter;
   }
   
   
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        if (mBluetoothLeService != null) {
        	//unbindService(mServiceConnection);
        	mBluetoothLeService = null;
        }
    }
	Handler mHideHandler = new Handler();



}
