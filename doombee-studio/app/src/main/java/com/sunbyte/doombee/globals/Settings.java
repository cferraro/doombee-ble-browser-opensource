package com.sunbyte.doombee.globals;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Settings {

	private final static String TRANSPORT_TYPE_KEY = "TransportType";
	private final static String TRANSPORT_ICE_KEY = "UseICE";
	
	private final static String TRANSPORT_STUN_KEY = "UseSTUN";
	private final static String TRANSPORT_STUN_SERVER_KEY = "STUNServer";
	private final static String TRANSPORT_STUN_PORT_KEY = "STUNPort";

	private final static String TRANSPORT_TLS_KEY = "UseTLS";
	private final static String TRANSPORT_TLS_SERVER_KEY = "TLSServer";
	private final static String TRANSPORT_TLS_PORT_KEY = "TLSPort";
		
//	private final static String SETTINGS_SHAREDPREFERENCES = "Settings";
	private static SharedPreferences SettingsPreferences;
	
	
	// UDP or FTP
	public static Integer GetTransportType(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return Integer.valueOf(SettingsPreferences.getString(TRANSPORT_TYPE_KEY, "1"));
	}
	
	public static Boolean GetUseICEbool(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return SettingsPreferences.getBoolean(TRANSPORT_ICE_KEY, false);
	}
	
	public static Boolean GetUseSTUNbool(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return SettingsPreferences.getBoolean(TRANSPORT_STUN_KEY, false);
	}
	
	public static Boolean GetUseTLSbool(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return SettingsPreferences.getBoolean(TRANSPORT_TLS_KEY, false);
	}

	public static String GetSTUNServerString(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return SettingsPreferences.getString(TRANSPORT_STUN_SERVER_KEY, "stun.vod.ftctele.com");
	}
	
	public static Integer GetSTUNPortInt(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return Integer.valueOf(SettingsPreferences.getString(TRANSPORT_STUN_PORT_KEY, "3478"));
	}
	
	public static String GetTLSServerString(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return SettingsPreferences.getString(TRANSPORT_TLS_SERVER_KEY, "");
	}
	
	public static Integer GetTLSPortInt(Context ctx) {
		SettingsPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		return Integer.valueOf(SettingsPreferences.getString(TRANSPORT_TLS_PORT_KEY, "5061"));
	}
	
	
	public static void SetTransportType(int value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putInt(TRANSPORT_TYPE_KEY, value);
			editor.commit();
		}
	}
	
	public static void SetUseICEbool(boolean value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putBoolean(TRANSPORT_ICE_KEY, value);
			editor.commit();
		}
	}
	
	public static void SetUseSTUNbool(boolean value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putBoolean(TRANSPORT_STUN_KEY, value);
			editor.commit();
		}
	}
	
	public static void SetUseTLSbool(boolean value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putBoolean(TRANSPORT_TLS_KEY, value);
			editor.commit();
		}
	}

	public static void SetSTUNServerString(String value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putString(TRANSPORT_STUN_SERVER_KEY, value);
			editor.commit();
		}
	}
	
	public static void SetSTUNPortInt(int value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putInt(TRANSPORT_STUN_SERVER_KEY, value);
			editor.commit();
		}
	}
	
	
	public static void SetTLSServerString(String value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putString(TRANSPORT_TLS_SERVER_KEY, value);
			editor.commit();
		}
	}
	
	public static void SetTLSPortInt(int value) {
		if (SettingsPreferences != null) {
			SharedPreferences.Editor editor = SettingsPreferences.edit();
			editor.putInt(TRANSPORT_TLS_PORT_KEY, value);
			editor.commit();
		}
	}
	
	
}
