package com.sunbyte.doombee.threads;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sunbyte.classes.BLEDevice;
import com.sunbyte.classes.UUIDInfo;
import com.sunbyte.doombee.globals.ApplicationStatus;
import com.sunbyte.doombee.rest.GetRequestException;
import com.sunbyte.doombee.rest.PostRequestException;
import com.sunbyte.doombee.rest.RestDictionary;
import com.sunbyte.doombee.rest.RestUtils;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class DNSRestThread extends AsyncTask<Void, Void, Void> {
	
	
	private final String RESTBase =  ApplicationStatus.ServiceBase;
	public static ProgressDialog dialog;
	
	Activity ctx;
	Context ctx2;
	String method;

	
	Boolean errorCheck;
	Boolean didfinish = false;
	
	BLEDevice bdevice;
	
	String resultXML;
	
	
	public String getXMLResult() {
		
		return this.resultXML;
	}
	
	public DNSRestThread(Activity parentActivity, String method, BLEDevice bdevice) {
		
		ctx = parentActivity;
		this.method = method;
		this.errorCheck = false;
		this.dialog = null;
		this.bdevice = bdevice;
		
		this.didfinish = false;
	
	}

	@Override
	protected void onPreExecute() {
		if (this.method == "get") {
			if (dialog == null) {
				dialog = ProgressDialog.show(ctx, "", "Resolving BLE DNS...\n");
			}
		}
		if (this.method == "post") {
			if (dialog == null) {
				dialog = ProgressDialog.show(ctx, "", "Loading...\nUpload changes");
			}
		}
	}
	
	
	@Override
	protected Void doInBackground(Void... params) {

		String result = "";
		Map<String, String> paramx = new HashMap<String, String>();
		
		UUIDInfo uinfo = this.bdevice.getUUIDInfo();
		String UUID = uinfo.getUUID();
		int minor = uinfo.getMinor();
		int major = uinfo.getMajor();
		String macAddress = this.bdevice.getDevice().getAddress();
		
		paramx.put("uuid", UUID);
		paramx.put("minor",  Integer.toString(minor));
		paramx.put("major", Integer.toString(major));
		paramx.put("MAC", macAddress);
		

		if (this.method == "get") {
			
			try {
				result = RestUtils.doGETRequest(
						this.RESTBase, paramx);
				System.out.println("RESULT IS : " + result);
			} catch (GetRequestException e) {
				System.out.println("Connection error!");
				
				e.printStackTrace();
		    	//dialog.dismiss();
		    	//Popups.ShowSimplePopup(ctx2, "Connection or server error!", "", "OK");
				this.errorCheck = true;
				
				this.didfinish = true;
				return null;
			}
			
			
			if (result != "") {
				this.resultXML = result;
				
				this.didfinish = true;
			} else {
				
				this.resultXML = null;
				System.out.println("GetTableStackItem: result empty");

				this.errorCheck = true;
		    	//dialog.dismiss();
		    	
		    	this.didfinish = true;
				return null;
			}
			
		}
		
		
		
		if (this.method == "set") {
			
				
			try {
				result = RestUtils.doPOSTRequest(
						this.RESTBase, paramx);
			} catch (PostRequestException e) {
				//Popups.ShowSimplePopup(ctx, "Connection or server error!", "", "OK");
				System.out.println("Webservice: Error saving data!");
				this.errorCheck = true;
		    	dialog.dismiss();
				e.printStackTrace();
			}	
		}
		
		
		return null;
	}
	
	
	public Boolean didThreadFinish() {
		return this.didfinish;
	}
	

    @Override
    protected void onPostExecute(Void unused) {

    	if (!this.errorCheck) {
    		dialog.dismiss();
    	
    	} else {
    		dialog.dismiss();
    		//dialog = ProgressDialog.show(ctx, "", "An error occurred while synchronizing: Possibly no internet connection or server down!");

    	}
    }
	
	
}