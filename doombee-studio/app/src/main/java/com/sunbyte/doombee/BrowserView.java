package com.sunbyte.doombee;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.openawt.draw.android.ShapePainter;

import com.sunbyte.classes.BLEDevice;
import com.sunbyte.classes.Circle;
import com.sunbyte.classes.CirclePacking;
import com.sunbyte.classes.NetworkEntity;
import com.sunbyte.doombee.ChromiumActivity;
import com.sunbyte.doombee.threads.DNSRestThread;
import com.sunbyte.parsers.XmlParser;

import kn.uni.voronoitreemap.datastructure.OpenList;
import kn.uni.voronoitreemap.diagram.PowerDiagram;
import kn.uni.voronoitreemap.j2d.Point2D;
import kn.uni.voronoitreemap.j2d.PolygonSimple;
import kn.uni.voronoitreemap.j2d.Site;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.View;

public class BrowserView extends SurfaceView implements View.OnTouchListener {

	public PowerDiagram diagram;
	OpenList sites = new OpenList();
	
	Canvas thiscanvas;
	Boolean fromHover = false;
	PolygonSimple hoverPoly;
	
	int width; // screen width
	int height; // screen height
	Display display;
	WindowManager wm;
	
	int minSites = 0;
	int numOfSites = 0;
	
	Context ctx;
	Activity activity;
	
	public Boolean DistancesGridActive;
	
	CirclePacking cp;
	
	int numOfDevices;
	ArrayList<BLEDevice> listOfDevices;
	
	
	public void RefreshBrowserDesignForNewDevice(int numOfDevices, 
									ArrayList<BLEDevice> listOfDevices) {
		
		
		
		if (numOfDevices < minSites) {
			numOfSites = minSites;
		} else {
			minSites = 30;
			
			numOfSites = numOfDevices;
			if (numOfSites < minSites) {
				numOfSites = minSites;
			}


			// TODO save sites status
			sites.clear();

			// starting VORONOI
			diagram = new PowerDiagram();
			
	
			Random rand = new Random();

			// create a root polygon which limits the voronoi diagram.
			//  here it is just a rectangle.
			PolygonSimple rootPolygon = new PolygonSimple();
			
			// circle packing for weighted tasselation. Loading Circles.
			cp = new CirclePacking(width, height);
			
			for (int i=0; i<numOfSites; i++)
			{
				int nextWidth = rand.nextInt(width);
				int nextHeight = rand.nextInt(height);
				int weight = rand.nextInt(10);
				if (weight == 0) weight = 1;
				System.out.println("WI " + nextWidth + " " + "HE " + nextHeight);
				cp.nodes.add(new Circle(nextWidth, nextHeight, "", weight));
			}
			
			cp.ChangeRadiusesByWeight();
			cp.packAllCircles();
			
			rootPolygon.add(0, 0);
			rootPolygon.add(width, 0);
			rootPolygon.add(width, height);
			rootPolygon.add(0, height);     
	
			ArrayList<Point2D> sitePoints = cp.getCirclePoints();

			// create 100 points (sites) and set random positions in the rectangle defined above.
		    for (int i = 0; i < numOfSites; i++) {
		    	
		    	Point2D sitepoint = sitePoints.get(i);
			    
		        Site site = new Site(sitepoint.x, sitepoint.y);

		        // we could also set a different weighting to some sites
		        site.setWeight(sitepoint.weight);

		        sites.add(site);
		    }
	
			// set the list of points (sites), necessary for the power diagram
			diagram.setSites(sites);
			// set the clipping polygon, which limits the power voronoi diagram
			diagram.setClipPoly(rootPolygon);
	
			// do the computation
			diagram.computeDiagram();   

		}
		// for each site we can no get the resulting polygon of its cell. 
		// note that the cell can also be empty, in this case there is no polygon for the corresponding site.
        for (int i=0;i<sites.size;i++){

            Site site=sites.array[i];
            PolygonSimple polygon=site.getPolygon();
            
            if (polygon != null) {
            	
            	// generate colors for polygon
            	if (listOfDevices.size() > i) {
            		
            		BLEDevice device = listOfDevices.get(i);
            		site.setDevice(device);
            		
            		
            		if (device.getColor() != null) {
            			polygon.setFillColor(device.getColor());
            		} else {
            			int newcolor = polygon.generateRandomColor(numOfDevices);
            			device.setColor(newcolor);
            			polygon.setFillColor(newcolor);
            		}
            		
	        	} else {
	        		// site without devices. set a disabled gray color
	        		polygon.setFillColor(Color.rgb(216, 216, 216)); // LTGray	
	        	}
            }
            
            site.setPolygon(polygon);

        }
        
       
		
		
	}
	
	
	public BrowserView(Context context, int numOfDevices, ArrayList<BLEDevice> listOfDevices, Activity baseActivity) {

		super(context);
		
		setWillNotDraw(false);
		
		this.setOnTouchListener(this);
		
		// get display
		this.wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		this.display = wm.getDefaultDisplay();

		width = display.getWidth();
		height = display.getHeight();
		
		ctx = context;
		activity = baseActivity;
		this.DistancesGridActive = true;
		
		this.numOfDevices = numOfDevices;
		this.listOfDevices = listOfDevices;

		this.RegenerateDiagram();
	
		        
	}
	
	
	public void RegenerateDiagram() {
		this.minSites = 0;
		this.RefreshBrowserDesignForNewDevice(numOfDevices, listOfDevices);
	}
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		thiscanvas = canvas;
		
		Point size = new Point();
		display.getSize(size);
		
		System.out.println("SIZE SCREEN " + size.x);
		if (size.x != width || size.y != height) {
			
			System.out.println("SIZE CHANGED " + width);
			width = size.x;
			height = size.y;
			
			this.RegenerateDiagram();
		}

		
		// TEST
		Paint p = new Paint();
		p.setColor(Color.GRAY);
		
		p.setStyle(Style.STROKE);
		p.setAntiAlias(true);
		// END TEST
		
		if (!this.fromHover) {
			
			diagram.setCanvas(canvas);
			diagram.showDiagram(DistancesGridActive);
			
			
		} else {
			diagram.setCanvas(canvas);
			diagram.showDiagram(DistancesGridActive);
			drawHoverSite(this.hoverPoly);
		
		}

		//thiscanvas.save();


	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP) {
			
			// loop through all polygons and check for clicked site
	        for (int i=0;i<sites.size;i++){
	            Site site=sites.array[i];
	            PolygonSimple polygon=site.getPolygon();
	            
	            Point2D p2d = new Point2D();
	            p2d.x = event.getRawX();
	            p2d.y = event.getRawY();
	            
	            
	            if (polygon.contains(p2d)) {

	            	if (event.getAction() == MotionEvent.ACTION_DOWN) { 
	            		
	            		System.out.println("ACTION DOWN");
	            		this.fromHover = true;
	            		this.hoverPoly = polygon;
	            		invalidate();
	            		return true;
	            	} 
	            	
	            	if (event.getAction() == MotionEvent.ACTION_UP) { 
	            		
	            		System.out.println("ACTION UP");
	            		this.fromHover = false;
	            		invalidate();
	            		
	            		
	            		// Resolve and start chromium
	            		
	            		Site selectedSite = site;
		            	final BLEDevice selectedDevice = selectedSite.getDevice();
		            	
		            	if (selectedDevice != null) {

		            		// perform request to DNS server in new thread
		            		final DNSRestThread newthread = new DNSRestThread(this.activity, "get", selectedDevice);
		            		newthread.execute();
		            		
		            		// check if thread is ready
		            		//Declare the timer
		            		final Timer t = new Timer();
		            		//Set the schedule function and rate
		            		t.scheduleAtFixedRate(new TimerTask() {

		            		    @Override
		            		    public void run() {
		            		    	if (newthread.didThreadFinish()) {
		            		    		
		            		    		System.out.println("Thread did finish");
		            		    		t.cancel();
		            		    		
		            		    		XmlParser xmlp = new XmlParser();
		            		    		List<NetworkEntity> netEntities = xmlp.ParseXMLResolvedBeacon(newthread.getXMLResult());
		            		    		
		            		    		
		            		    		System.out.println("ENTITY SIZE" + netEntities.size());
		            		    		// start activity
		            		    		
		            		    		Intent intent = new Intent(ctx, ChromiumActivity.class);
		            		    		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		            		    		if (netEntities.size() > 0) {
		            		    			intent.putExtra("ResolvedURI", netEntities.get(0).getfullURI());
		            		    		} else {
		            		    			
		            		    			intent.putExtra("ResolvedURI", "");
		            		    		}
		            		    	    intent.putExtra("DeviceName", selectedDevice.getDevice().getName());
		            		    	    intent.putExtra("DeviceAddress", selectedDevice.getDevice().getAddress());
		            		    	    intent.putExtra("DeviceUUID", selectedDevice.getUUIDInfo().getUUID());
		            		    	    intent.putExtra("DeviceMinor", Integer.toString(selectedDevice.getUUIDInfo().getMinor()));
		            		    	    intent.putExtra("DeviceMajor", Integer.toString(selectedDevice.getUUIDInfo().getMajor()));
		            		    	    
		            		    	    
		            		    	    ctx.startActivity(intent);

		            		    		
		            		    	}
		            		        //Called each time when 1000 milliseconds (1 second) (the period parameter)
		            		    }

		            		},
		            		//Set how long before to start calling the TimerTask (in milliseconds)
		            		0,
		            		//Set the amount of time between each execution (in milliseconds)
		            		1000);
		            		
		            	
		            	}

	            	} 
	            	// draw hover effect
	            	break;
	            }
	            
	        }
		}
		return false;
	}
	
	
	
	public int darker (int color, float factor) {
	    int a = Color.alpha( color );
	    int r = Color.red( color );
	    int g = Color.green( color );
	    int b = Color.blue( color );

	    return Color.argb( a,
	            Math.max( (int)(r * factor), 0 ),
	            Math.max( (int)(g * factor), 0 ),
	            Math.max( (int)(b * factor), 0 ) );
	}
	
	
	public void drawHoverSite(PolygonSimple poly) {
		
		if (poly != null) {
			
			System.out.println("Drawing Hover");
			Paint p = new Paint();
		
			int darkerColor = this.darker(poly.fillColor, 0.75f);
			p.setColor(darkerColor);
			
			p.setStyle(Style.FILL);
			p.setAntiAlias(true);
			ShapePainter.draw(thiscanvas, poly, p);
			
			p = new Paint();
			p.setColor(Color.GRAY);
			p.setStrokeWidth(5.5f);
			p.setStyle(Style.STROKE);
			p.setAntiAlias(true);
			ShapePainter.draw(thiscanvas, poly, p);

		} else {
			System.out.println("Poly null of");
		}
		
	}

}
