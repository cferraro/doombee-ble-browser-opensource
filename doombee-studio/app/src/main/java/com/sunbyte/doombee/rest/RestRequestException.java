package com.sunbyte.doombee.rest;

public class RestRequestException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String _name;
	
	// constructor
	public RestRequestException(String message, String name) {
		 
		 super(message); 
		 _name = name;
    } 

	public String getCustomName() {
		 return _name;
	}
		 

}
