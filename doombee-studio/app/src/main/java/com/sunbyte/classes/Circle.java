package com.sunbyte.classes;

import android.graphics.Canvas;
import android.graphics.Paint;



public class Circle {
	  public float x;
	  public float y;
	  public float r;
	  public int weight;
	  
	  String label;

	  public Circle(float _x, float _y, String _label, int _weight)
	  {
	    this.x = _x;
	    this.y = _y;
	    this.label = _label;
	    this.weight = _weight;
	  }

	  float distance(float _x1, float _y1, float _x2, float _y2)
	  {
	    return (float) Math.sqrt((_x1-_x2)*(_x1-_x2)+(_y1-_y2)*(_y1-_y2));
	  }

	  float getOffset(float _x, float _y)
	  {
	    return distance(this.x, this.y, _x, _y);
	  }

	  boolean contains(float _x, float _y)
	  {
	    return distance(this.x, this.y, _x, _y) <= this.r;
	  }

	  boolean intersect(Circle _circle)
	  {
	    float d = distance(this.x, this.y, _circle.x, _circle.y);
	    return d <= (this.r + _circle.r);
	  }

	  void draw(Canvas c, Paint p)
	  {
		  //System.out.println("DRAW CIRCLE");
		 c.drawCircle(x, y, r, p);
	    //ellipse(x, y, r*2, r*2);
	  }
	}