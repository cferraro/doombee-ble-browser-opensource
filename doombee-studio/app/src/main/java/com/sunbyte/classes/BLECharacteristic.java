package com.sunbyte.classes;

import java.util.List;

import android.bluetooth.BluetoothGattCharacteristic;

public class BLECharacteristic {



	public String UUID;
	public String Name;
	public BluetoothGattCharacteristic gattCharacteristic;

	public String Value;

	/*
	* Getters and setters
	 */

	public String getUUID() {
		return UUID;
	}
	
	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
	
	public String getName() {
		return this.Name;
	}
	
	public void setName(String name) {
		this.Name = name;
	}

	public String getValue() {
		return this.Value;
	}

	public void setValue(String value) {
		this.Value = value;
	}
	
	public BluetoothGattCharacteristic getgattCharacteristic() {
		return this.gattCharacteristic;
	}
	
	public void setgattCharacteristic(BluetoothGattCharacteristic gattCharacteristic) {
		this.gattCharacteristic = gattCharacteristic;
	}
	
}
