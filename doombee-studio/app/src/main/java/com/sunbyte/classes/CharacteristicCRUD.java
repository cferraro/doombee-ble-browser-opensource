package com.sunbyte.classes;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

import com.sunbyte.services.BluetoothLeService;

import android.bluetooth.BluetoothGattCharacteristic;

public class CharacteristicCRUD {
	
	public ArrayList<BLECharacteristic> outValues;
	public ArrayList<BLECharacteristic> inValues;
	private BluetoothLeService mBluetoothLeService;

	boolean isCompleted;
	boolean isReading;
	boolean isWriting;
	ReadLock readLock;
	private ReentrantReadWriteLock readWriteLock;
	
	public CharacteristicCRUD(BluetoothLeService mBLS) {
		this.mBluetoothLeService = mBLS;
		isReading = false;
		isCompleted = false;
		readWriteLock = new ReentrantReadWriteLock();
		this.readLock = readWriteLock.readLock();
	}
	
	
	public void setAsyncInputIsReady(boolean value) {

	    synchronized (readLock) {
	        isReading = false;
	        readLock.notifyAll();
	    }
	}

	public boolean getCompletedStatus() {
		return this.isCompleted;
	}

	public void setNewData(String value, String UUID) {

		for (int i = 0; i < this.inValues.size(); i++) {
			if (this.inValues.get(i).getUUID().equals(UUID)) {

				this.inValues.get(i).setValue(value);
				break;
			}
		}

	}


	public void writeBLECharacteristic(BLECharacteristic charac) {

		BluetoothGattCharacteristic gattc = charac.getgattCharacteristic();
		if (isCharacteristicWriteable(gattc)) {

			String valueToWrite = charac.getValue().toString();
			isWriting = this.mBluetoothLeService.writeCharacteristic(gattc, valueToWrite);

			System.out.println("Writing  " + charac.getUUID());

			// locking thread until done
			while (isWriting) {
				synchronized (readLock) {
					try {
						readLock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			isCompleted = true;

		}


	}

	public void writeAllCharacteristicsFromList(ArrayList<BLECharacteristic> list) {

		this.inValues = list;

		for (int i = 0; i < list.size(); i++) {

			BluetoothGattCharacteristic gattc = list.get(i).getgattCharacteristic();
			if (isCharacteristicWriteable(gattc)) {

				String valueToWrite = list.get(i).getValue().toString();
				isWriting = this.mBluetoothLeService.writeCharacteristic(gattc, valueToWrite);

				System.out.println("Writing " + i + "  " + list.get(i).getUUID());
				while (isWriting) {
					synchronized (readLock) {
						try {
							readLock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				isCompleted = true;

			}
		}

		isCompleted = true;

	}
	

	public void readBLECharacteristic(BLECharacteristic charac) {
		BluetoothGattCharacteristic gattc = charac.getgattCharacteristic();
		if (isCharacterisitcReadable(gattc)) {

		}

	}

	public void readAllCharacteristicsFromList(ArrayList<BLECharacteristic> list) {

		this.inValues = list;

		for (int i = 0; i < list.size(); i++) {


			BluetoothGattCharacteristic gattc = list.get(i).getgattCharacteristic();
			if (isCharacterisitcReadable(gattc)) {

				isReading = this.mBluetoothLeService.readCharacteristic(gattc);

				System.out.println("Reading " + i + "  " + list.get(i).getUUID());
				while (isReading) {
					synchronized (readLock) {
						try {
							readLock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				isCompleted = true;

			}
		}

		isCompleted = true;

	}

	public static boolean isCharacteristicWriteable(BluetoothGattCharacteristic pChar) {
		return (pChar.getProperties() & (BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) != 0;
	}

	/**
	 * @return Returns <b>true</b> if property is Readable
	 */
	public static boolean isCharacterisitcReadable(BluetoothGattCharacteristic pChar) {
		return ((pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) != 0);
	}

	/**
	 * @return Returns <b>true</b> if property is supports notification
	 */
	public static boolean isCharacterisiticNotifiable(BluetoothGattCharacteristic pChar) {
		return (pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
	}

}
