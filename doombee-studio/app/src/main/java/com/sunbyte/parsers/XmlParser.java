package com.sunbyte.parsers;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import com.sunbyte.classes.NetworkEntity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


public class XmlParser {

	//private static ArrayList<Category> Categorys;
	public static Bitmap loadPic(String url) {
		
		// the downloaded thumb (none for now!)
		Bitmap pic = null;

		// sub-sampling options
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;

		try {

			// open a connection to the URL
			// Note: pay attention to permissions in the Manifest file!
			URL u = new URL(url);
			URLConnection c = u.openConnection();
			c.connect();
			
			// read data
			BufferedInputStream stream = new BufferedInputStream(c.getInputStream());

			// decode the data, subsampling along the way
			pic = BitmapFactory.decodeStream(stream, null, opts);

			// close the stream
			stream.close();

		} catch (MalformedURLException e) {
			Log.e("Threads03", "malformed url: " + url);
		} catch (IOException e) {
			Log.e("Threads03", "An error has occurred downloading the image: " + url);
		}

		// return the fetched thumb (or null, if error)
		return pic;
	}
	
	private static Document ParseXMLString(String xmlString) {
		
		// fix encoding problem
		xmlString = xmlString.replaceAll("[^\\x20-\\x7e]", "");
		
		DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		Document doc = null;
		
		try {
			InputSource inStream = new InputSource(); 
			inStream.setCharacterStream(new StringReader(xmlString));
			inStream.setEncoding("windows-1252");
			
			db = factory.newDocumentBuilder();
			doc = db.parse(inStream);
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	
		return doc;
	}
	
	
	
	
	public static List<NetworkEntity> ParseXMLResolvedBeacon(String resultDNSStr) {
		
		Document doc = ParseXMLString(resultDNSStr);

		List<NetworkEntity> ListOfNetworks = new ArrayList<NetworkEntity>();
		NodeList nodeList = doc.getElementsByTagName("Network");

		System.out.println("NODELIST LENGTH" + nodeList.getLength());

		for (int i = 0; i < nodeList.getLength(); i++) {
			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				NetworkEntity newNetwork = new NetworkEntity();
			
				NodeList idNetworkId = element.getElementsByTagName("id");
				
				if (idNetworkId != null && idNetworkId.item(0) != null) {
					
					if (idNetworkId.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element idNetworkElement = (Element)idNetworkId.item(0);
						String idValue = idNetworkElement.getFirstChild().getNodeValue().trim();
						
						newNetwork.setId(Integer.valueOf(idValue));
					}
	
					NodeList domain = element.getElementsByTagName("domain");
					if (domain.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element domainElement = (Element)domain.item(0);
						
						String domainValue;
						if (domainElement.getFirstChild() != null) {
							domainValue = domainElement.getFirstChild().getNodeValue().trim();
						} else {
							domainValue = "";
						}
						newNetwork.setdomain(domainValue);
					}
					
					NodeList fullUri = element.getElementsByTagName("fullURI");
					if (fullUri.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element fullUriElement = (Element)fullUri.item(0);
						
						String fullUriValue;
						if (fullUriElement.getFirstChild() != null) {
							fullUriValue = fullUriElement.getFirstChild().getNodeValue().trim();
						} else {
							fullUriValue = "";
						}
						newNetwork.setfullURI(fullUriValue);
					}
					
					
					NodeList macaddress = element.getElementsByTagName("MACAddress");
					if (macaddress.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element macElement = (Element) macaddress.item(0);
						
						String macValue;
						if (macElement.getFirstChild() != null) {
							macValue = macElement.getFirstChild().getNodeValue().trim();
						} else {
							macValue = "";
						}
						newNetwork.setMACAddress(macValue);
					}
					
					NodeList uuid = element.getElementsByTagName("UUID");
					if (uuid.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element uuidElement = (Element) uuid.item(0);
						
						String uuidValue;
						if (uuidElement.getFirstChild() != null) {
							uuidValue = uuidElement.getFirstChild().getNodeValue().trim();
						} else {
							uuidValue = "";
						}
						newNetwork.setUUID(uuidValue);
					}
					
					NodeList major = element.getElementsByTagName("Major");
					if (major.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element majorElement = (Element)major.item(0);
						String majorValue = majorElement.getFirstChild().getNodeValue().trim();
						
						newNetwork.setMajor(Integer.valueOf(majorValue));
					}
					
					NodeList minor = element.getElementsByTagName("Minor");
					if (minor.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element minorElement = (Element)minor.item(0);
						String minorValue = minorElement.getFirstChild().getNodeValue().trim();
						
						newNetwork.setMajor(Integer.valueOf(minorValue));
					}
					
					NodeList iconuri = element.getElementsByTagName("iconURI");
					if (iconuri.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element iconuriElement = (Element) iconuri.item(0);
						
						String iconuriValue;
						if (iconuriElement.getFirstChild() != null) {
							iconuriValue = iconuriElement.getFirstChild().getNodeValue().trim();
						} else {
							iconuriValue = "";
						}
						newNetwork.setUUID(iconuriValue);
					}
					
					NodeList color = element.getElementsByTagName("color");
					if (color.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element colorElement = (Element) color.item(0);
						
						String colorValue;
						if (colorElement.getFirstChild() != null) {
							colorValue = colorElement.getFirstChild().getNodeValue().trim();
						} else {
							colorValue = "";
						}
						newNetwork.setUUID(colorValue);
					}
					
					
					NodeList rank = element.getElementsByTagName("rank");
					if (rank.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element rankElement = (Element)rank.item(0);
						String rankValue = rankElement.getFirstChild().getNodeValue().trim();
						
						newNetwork.setRank(Integer.valueOf(rankValue));
					}
					
					NodeList gpslocation = element.getElementsByTagName("gpslocation");
					if (gpslocation.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element gpslocationElement = (Element) gpslocation.item(0);
						
						String gpslocationValue;
						if (gpslocationElement.getFirstChild() != null) {
							gpslocationValue = gpslocationElement.getFirstChild().getNodeValue().trim();
						} else {
							gpslocationValue = "";
						}
						newNetwork.setUUID(gpslocationValue);
					}
					
					NodeList created = element.getElementsByTagName("created");
					if (created.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element createdElement = (Element) created.item(0);
						String createdValue = createdElement.getFirstChild().getNodeValue().trim();
						
						System.out.println("parse network " + createdValue);
						newNetwork.setcreated(createdValue);
					}
					
					
					NodeList modified = element.getElementsByTagName("modified");
					if (modified.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element modifiedElement = (Element) modified.item(0);
						String modifiedValue = modifiedElement.getFirstChild().getNodeValue().trim();
						
						System.out.println("parse network " + modifiedElement);
						newNetwork.setmodified(modifiedValue);
					}
					
					
					ListOfNetworks.add(newNetwork);
				}
			}
		}
		
		
		return ListOfNetworks;
	}
	
	
	
	
	
}
